<?php

namespace App\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\ValidationRuleParser;

class ValidationServiceProvider extends ServiceProvider
{
    protected $params = [], $attrs = [];
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('phone', function($attribute, $value, $parameters)
        {
            return preg_match('/^(09)[0-9]{9}$/', $value) && strlen($value) == 11;
        });
    }
}
