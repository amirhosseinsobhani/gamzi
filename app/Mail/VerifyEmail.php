<?php

namespace App\Mail;

use App\Modules\User\Models\Verify;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $verify, $text;

    /**
     * Create a new message instance.
     *
     * @param Verify $verify
     * @param string $text
     */
    public function __construct(Verify $verify, string $text)
    {
        $this->verify = $verify;
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.verify')
                    ->with([
                        'code' => $this->verify->code,
                        'text' => $this->text,
                    ]);
    }
}
