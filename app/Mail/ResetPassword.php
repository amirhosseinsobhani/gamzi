<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $new_pass, $text;

    /**
     * Create a new message instance.
     *
     * @param $new_pass
     * @param $text
     */
    public function __construct($new_pass, $text)
    {
        $this->new_pass = $new_pass;
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('password.reset-pass')
            ->with([
                'new_pass' => $this->new_pass,
                'text' => $this->text,
            ]);
    }
}
