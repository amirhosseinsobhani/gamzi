<?php


namespace App\Http\Traits;


use Illuminate\Support\Facades\Validator;
use Tests\CreatesApplication;

trait AppTools
{

    use CreatesApplication;

    public $header = [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods' => 'POST,GET,PATCH,OPTIONS,PUT,DELETE',
        'Content-Type' => 'application/json',
        'Access-Control-Allow-Headers' => 'ContentType, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With'
    ];

    public function _checkValidation($data, $config)
    {
        $validator = Validator::make(($data), $config);
        if ($validator->fails()) {
            response()->json([
                'result' => false,
                'message' => $validator->errors()->first(),
            ], 400, $this->header)->send();
            die;

        }
        return true;
    }

    protected function __outPut($response, $status = 200, $option = 256)
    {
        $data = [];
        $data['result'] = true;

        if ($status > 300)
            $data['result'] = false;

        if (isset($response['result'])) {
            $data['result'] = $response['result'];
            unset($response['result']);
        }

        if (!isset($response['message'])) {
            $data['message'] = null;
        } else {
            $data['message'] = $response['message'];
            unset($response['message']);
        }
        if (isset($response['data'])) {
            $data['data'] = $response['data'];
        } else {
            $data['data'] = $response;
        }

        response()->json($data, $status, $this->header, $option)->send();
        die;
    }

    protected function UploadImage($file, $path)
    {
        error_reporting(E_ERROR | E_WARNING | E_PARSE);
        $imagePath = public_path('upload/' . $path);
        $fileName = $file->getClientOriginalName();
        $file->move($imagePath, $fileName);
        $url = env('ZIREH_URL') . env('APP_DIRECTORY') . '/public/upload/' . $path . '/' . $fileName;
        return $url;
    }

    protected function __testOutPut($path, $method, $data = [])
    {
        return $header = $this->withHeaders([
            'X-Header' => 'Value',
            'token' => env('TOKEN')
        ])->json($method, $path, $data);
    }

    //build condition for eloquent
    protected function _condition(array $data, array $items, string $operator = '=')
    {
        $condition = [];
        foreach ($items as $item) {
            if (isset($data[$item])) {
                if (strtolower($operator) == 'like')
                    $condition[] = [$item, $operator, '%' . $data[$item] . '%'];
                else
                    $condition[] = [$item, $operator, $data[$item]];
            }
        }
        return $condition;
    }

    protected function _filter(array $data, array $filter, string $status = 'normal')
    {
//        $filter = ['text', 'like', 'name'];
//        $filter = ['age', '>', 'age_s'];
        $condition = [];
//        $this->__outPut([$data[$filter[0][2]]]);

        foreach ($filter as $item) {
            if (isset($data[$item[0]])) {
                $operator = $item[1];
                $item_data = $data[$item[0]];

                if (count($item) >= 3)
                    $column_name = $item[2];
                else
                    $column_name = $item[0];

                if ($column_name == 'age')
                    $this->_age($data[$item[0]]);

                if (strtolower($item[1]) == 'like')
                    $condition[] = [$column_name, $operator, '%' . $item_data . '%'];
                else
                    $condition[] = [$column_name, $operator, $item_data];
            } elseif ($status != 'normal') {
                if (isset($data[$item[2]])) {

                    $operator = $item[1];
                    $item_data = $data[$item[2]];

                    if (count($item) >= 3)
                        $column_name = $item[0];
                    else
                        $column_name = $item[2];

                    if ($column_name == 'age')
                        $item_data = $this->_age($data[$item[2]]);

                    if (strtolower($item[1]) == 'like')
                        $condition[] = [$column_name, $operator, '%' . $item_data . '%'];
                    else
                        $condition[] = [$column_name, $operator, $item_data];
                }
            }
        }
        return $condition;
    }

    protected function _age($birthYear)
    {
        return date('Y') - $birthYear;
    }

    protected function _array_key_push(array $array, string $key, $value)
    {
        foreach ($array as $k => $item) {
            $array[$k][$key] = $value;
        }
        return $array;
    }


}
