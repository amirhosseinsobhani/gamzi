<?php


namespace App\Http\Traits;


use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Model;

trait Who
{
    use AppTools;

    public static function User()
    {
        $request = app('request');
        $headers = $request->headers->all();
        if(empty($headers[ 'token' ]))
            return null;

        $token = $headers[ 'token' ][ 0 ];
        $decrypted_token = decrypt($token);

        $user = User::find($decrypted_token['user']);

        if(!empty($field)) {
            if(!isset($user->$field)) {
                return null;
            } else if(!empty($user->$field)) {
                return $user->$field;
            }
        }
        return $user;
    }

    public static function checkOwner(Model $model)
    {
        if ($model->user_id == Who::User()->id)
            return true;
        else {
            response()
                ->json(['result' => false, 'message' => 'عدم دسترسی', 'data' => []], 406)
                ->send();
            die;
        }
    }
}
