<?php

namespace App\Http\Models;

use App\Http\Traits\AppTools;
use Illuminate\Database\Eloquent\Model;

class GModel extends Model
{
    use AppTools;
}
