<?php

namespace App\Http\Controllers;

use App\Http\Traits\AppTools;
use App\Http\Traits\Who;
use App\Modules\Client\Models\Gnet;
use App\Modules\User\Models\Token;
use Illuminate\Validation\Rule;

class GameziController extends Controller
{
    use AppTools;

    protected $Guard = true;
    protected $gnet_id_required = true;
    protected $gnet;
    protected $user;

    public function __construct()
    {
        if ($this->Guard) {
            $this->__Guard();
            $this->user = Who::User();
        }

        if ($this->gnet_id_required) $this->gnetIdRequired();
    }

    protected function __Guard()
    {
        $request = app('request');
        $headers = $request->headers->all();

        if (empty($headers['token'])) {
            $this->__outPut(['message' => 'Not Found Token!'], 401);
            return false;
        }

        $token = $headers['token'][0];
        $Token = new Token();

        if (!$Token->isValid($token)) {
            $this->__outPut(['message' => 'inValid Token!'], 401);
            return false;
        }

        return true;
    }

    protected function gnetIdRequired()
    {
        if (isset(app('request')->gnet_id))
            $this->gnet = $this->gnetOwner(app('request')->gnet_id);
        else
            $this->__outPut(['message' => 'شناسه گیمنت ارسال نشده است'], 400);
    }

    protected function _uploadFile($file, string $path): string
    {
        error_reporting(E_ERROR | E_WARNING | E_PARSE);
        $imagePath = public_path('upload/' . $path);
        $file_ext = strtolower($file->getClientOriginalExtension());
        $date = '(' . date('Y-m-d H-i-s') . ')';
        $fileName = $file->getClientOriginalName();
        $fileName = explode('.', $fileName)[0] . $date . '.' . $file_ext;
        $file->move($imagePath, $fileName);
        $url = env('APP_URL') . env('APP_DIRECTORY') . '/public/upload/' . $path . '/' . $fileName;
        return $url;
    }

    protected function gnetOwner(int $gnet_id) : Gnet
    {
        $this->_checkValidation(['gnet_id' => $gnet_id], [
            'gnet_id' => [
                'required',
                Rule::exists(App(Gnet::class)->getTable(), 'id')->where(function ($query) {
                    $query->where('user_id', $this->user->id);
                })
            ]
        ]);
        return Gnet::find($gnet_id);
    }
}
