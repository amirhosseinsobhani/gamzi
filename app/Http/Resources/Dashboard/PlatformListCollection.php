<?php

namespace App\Http\Resources\Dashboard;

use App\Modules\Game_Platform\Models\GnetPlatformGame;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PlatformListCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array|\Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($gnetPlatformGame) {
            return (new PlatformListResource($gnetPlatformGame));
        });
    }
}
