<?php

namespace App\Http\Resources\Dashboard;

use Illuminate\Http\Resources\Json\JsonResource;

class PlatformListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'gnet_platform_id' => $this['id'],
            'alias' => $this['alias'],
            'gnet_id' => $this['gnet_id'],
            'is_active' => $this['is_active'],
            'work_user' => @$this['work_trans_action'][0]['work_user'] ?: [],
        ];
    }
}
