<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $domains = [
            'http://gamezi.ir',
            'https://gamezi.ir',
            'http://test.gamezi.ir',
            'https://test.gamezi.ir',
            'http://main.gamezi.ir',
            'https://main.gamezi.ir',
        ];

        if (isset($request->server()['HTTP_ORIGIN'])) {
            $origin = $request->server()['HTTP_ORIGIN'];
            if (in_array($origin, $domains)) {
                header('Access-Control-Allow-Origin : *');
            }
            header("Access-Control-Allow-Headers : ContentType, Content-Type, Authorization, X-Requested-With, token");
            header('Access-Control-Allow-Methods : POST, GET, PATCH, OPTIONS, PUT, DELETE');
        }
        return $next($request);
    }
}
