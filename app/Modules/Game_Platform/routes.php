<?php

use Illuminate\Support\Facades\Route;

$module_namespace = "App\Modules\Game_Platform\Controllers";

Route::prefix('api')->middleware('api')->namespace($module_namespace)->group(function () {
    Route::prefix('site')->group(function () {
        Route::prefix('platform')->group(function () {
            Route::post('/', 'Site\PlatformController@insert');
            Route::get('{platform}', 'Site\PlatformController@get');
            Route::post('list', 'Site\PlatformController@list');
            Route::delete('{platform}', 'Site\PlatformController@delete');
            Route::post('update/{platform}', 'Site\PlatformController@update');
        });

        Route::prefix('developer')->group(function () {
            Route::post('/', 'Site\DeveloperController@insert');
            Route::get('{developer}', 'Site\DeveloperController@get');
            Route::post('update/{developer}', 'Site\DeveloperController@update');
            Route::post('list', 'Site\DeveloperController@list');
            Route::delete('{developer}', 'Site\DeveloperController@delete');
        });

        Route::prefix('publisher')->group(function () {
            Route::post('/', 'Site\PublisherController@insert');
            Route::get('{publisher}', 'Site\PublisherController@get');
            Route::post('update/{publisher}', 'Site\PublisherController@update');
            Route::post('list', 'Site\PublisherController@list');
            Route::delete('{publisher}', 'Site\PublisherController@delete');
        });

        Route::prefix('game')->group(function () {
            Route::post('/', 'Site\GameController@insert');
            Route::get('{game}', 'Site\GameController@get');
            Route::delete('{game}', 'Site\GameController@delete');
            Route::post('update/{game}', 'Site\GameController@update');
            Route::post('list', 'Site\GameController@list');
        });
    });

    Route::prefix('client')->group(function () {
        Route::prefix('platform')->group(function () {
            Route::post('/', 'Gnet\PlatformController@add');
            Route::post('list', 'Gnet\PlatformController@list');
            Route::post('game/detail', 'Gnet\PlatformController@get');
            Route::post('remove', 'Gnet\PlatformController@remove');
        });
        Route::prefix('game')->group(function () {
            Route::prefix('custom')->group(function () {
                Route::post('/', 'Gnet\CustomGameController@add');
                Route::post('remove', 'Gnet\CustomGameController@remove');
                Route::post('get', 'Gnet\CustomGameController@get');
                Route::post('list', 'Gnet\CustomGameController@list');
            });

            Route::post('/', 'Gnet\GameController@add');
            Route::post('platform', 'Gnet\GameController@addGameToPlatform');
            Route::post('platform/list', 'Gnet\GameController@platformGameList');
            Route::post('platform/remove', 'Gnet\GameController@removeGameFromPlatform');

        });
    });
});
