<?php


namespace App\Modules\Game_Platform\Models;


use App\Modules\Client\Models\Gnet;
use App\Modules\History\Models\GnetWorkUser;
use Illuminate\Database\Eloquent\Model;

class CustomGame extends Model
{
    protected $table = 'gnet_custom_game';

    protected $fillable = [
        'gnet_id',
        'company',
        'alias',
        'description',
        'image',
        'age_rating',
        'number_player',
        'admin_status',
    ];

    public function gnet()
    {
        return $this->belongsTo(Gnet::class, 'gnet_id');
    }

    public function workDevice()
    {
        return $this->hasMany(GnetWorkUser::class, 'gnet_custom_game_id');
    }
}
