<?php


namespace App\Modules\Game_Platform\Models;


use App\Http\Models\GModel;

class PlatformCategory extends GModel
{
    protected $table = 'platform_category';

    protected $fillable = [
        'title',
        'fa_title',
        'icon',
    ];

    public $timestamps = false;

    public function platform()
    {
        return $this->hasMany(Platform::class, 'platform_category_id');
    }

}
