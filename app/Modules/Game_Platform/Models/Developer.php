<?php


namespace App\Modules\Game_Platform\Models;


use App\Http\Models\GModel;

class Developer extends GModel
{
    protected $table = 'developer';

    protected $fillable = [
        'rawg_id',
        'alias',
        'name',
        'logo',
        'description',
        'established_date'
    ];

    public function game()
    {
        return $this->belongsToMany(Game::class, 'game_developer');
    }
}
