<?php


namespace App\Modules\Game_Platform\Models;


use App\Http\Models\GModel;

class Media extends GModel
{
    protected $table = 'media';

    const ImageType = 0, VideoType = 1;

    protected $fillable = [
        'title',
        'type',
        'url',
        'preview'
    ];

    public function gameImage()
    {
        return $this->belongsToMany(Game::class, 'media_game')->where('type', self::ImageType);
    }

    public function gameClip()
    {
        return $this->belongsToMany(Game::class, 'media_game')->where('type', self::VideoType);
    }

}
