<?php


namespace App\Modules\Game_Platform\Models;


use App\Http\Models\GModel;
use App\Modules\History\Models\GnetWorkTransaction;

class GnetPlatformGame extends GModel
{
    protected $table = 'gnet_platform_game';

    const Active = 1, DeActive = 0;

    protected $fillable = [
        'gnet_id',
        'platform_id',
        'game_id',
        'parent_id',
        'alias',
        'is_active',
    ];

    public $timestamps = false;

    public function platform()
    {
        return $this->belongsTo(Platform::class);
    }

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    public function workTransAction()
    {
        return $this->belongsToMany(GnetWorkTransaction::class, 'gnet_work_transaction_platform', 'gnet_platform_id', 'gnet_work_transaction_id')
                    ->withPivot('gnet_id');
    }

}
