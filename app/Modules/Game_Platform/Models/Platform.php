<?php


namespace App\Modules\Game_Platform\Models;


use App\Modules\Client\Models\Gnet;
use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    protected $table = 'platform';

    protected $fillable = [
        'rawg_id',
        'platform_category_id',
        'name',
        'alias',
        'description',
        'release_time',
        'image'
    ];

    public $timestamps = false;

    public function gnet()
    {
        return $this->belongsToMany(Gnet::class, 'gnet_platform_game', 'platform_id', 'gnet_id');
    }

    public function game()
    {
        return $this->belongsToMany(Game::class, 'game_platform');
    }

    public function category()
    {
        return $this->belongsTo(PlatformCategory::class, 'platform_category_id');
    }
}
