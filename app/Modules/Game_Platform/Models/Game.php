<?php


namespace App\Modules\Game_Platform\Models;


use App\Modules\Client\Models\Gnet;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'game';

    protected $fillable = [
        'rawg_id',
        'esrb_rating_id',
        'alias',
        'name',
        'description',
        'description_en',
        'metacritic',
        'metacritic_url',
        'background_image',
        'rating',
        'rating_top',
        'playtime',
        'website',
        'image',
        'release_time',
        'use_count',
        'tags'
    ];

    public function developer()
    {
        return $this->belongsToMany(Developer::class, 'game_developer', 'game_id');
    }

    public function publisher()
    {
        return $this->belongsToMany(Publisher::class, 'game_publisher', 'game_id');
    }

    public function genre()
    {
        return $this->belongsToMany(Genre::class, 'game_genre', 'game_id');
    }

    public function gnet()
    {
        return $this->belongsToMany(Gnet::class, 'gnet_platform_game', 'game_id', 'gnet_id')
            ->withPivot('alias', 'parent_id', 'is_active');
    }

    public function esrbRating()
    {
        return $this->belongsTo(EsrbRating::class, 'esrb_rating_id');
    }
}
