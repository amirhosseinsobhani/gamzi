<?php


namespace App\Modules\Game_Platform\Models;


use App\Http\Models\GModel;

class EsrbRating extends GModel
{
    protected $table = 'esrb_rating';

    protected $fillable = [
        'title',
        'icon',
        'start_age',
        'description',
    ];

    public function game()
    {
        return $this->hasMany(Game::class);
    }
}
