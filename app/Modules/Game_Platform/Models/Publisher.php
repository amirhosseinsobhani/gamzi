<?php


namespace App\Modules\Game_Platform\Models;


use App\Http\Models\GModel;

class Publisher extends GModel
{
    protected $table = 'publisher';

    protected $fillable = [
        'rawg_id',
        'alias',
        'name',
        'logo',
        'description',
        'established_date',
    ];

    public function game()
    {
        return $this->belongsToMany(Game::class, 'game_publisher', 'publisher_id', 'game_id');
    }

}
