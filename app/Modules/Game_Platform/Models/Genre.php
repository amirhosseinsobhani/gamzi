<?php


namespace App\Modules\Game_Platform\Models;


use App\Http\Models\GModel;

class Genre extends GModel
{
    protected $table = 'genre';

    protected $fillable = [
        'title',
        'fa_title'
    ];

    public $timestamps = false;

    public function game()
    {
        return $this->belongsToMany(Game::class, 'game_genre', 'genre_id', 'game_id');
    }

}
