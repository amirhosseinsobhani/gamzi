<?php


namespace App\Modules\Game_Platform\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Modules\Game_Platform\Models\CustomGame;
use App\Modules\History\Models\GnetWorkDevice;
use Illuminate\Http\Request;

class CustomGameController extends GameziController
{
    public function add(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'company' => 'string|max:50|nullable',
            'alias' => 'string|max:50',
            'description' => 'string|max:150|nullable',
            'age_rating' => 'numeric|max:120|nullable',
            'number_player' => 'numeric|max:1000|nullable',
            'image' => 'file|mimes:png,jpeg,jpeg|max:1024|nullable'
        ]);


        if (isset($inputs['image']))
            $inputs['image'] = $this->_uploadFile($request->file('image'), 'custom-game');

        CustomGame::create($inputs);

        $this->__outPut(['message' => 'بازی با موفقیت ثبت شد']);
    }

    public function remove(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'custom_game_id' => 'required|exists:' . App(CustomGame::class)->getTable() . ',id'
        ]);

        $custom_game = CustomGame::find($inputs['custom_game_id']);
        $work_device = $custom_game->workDevice()->where('flg_online', GnetWorkDevice::FlgOnline)->count();
        if ($work_device > 0)
            $this->__outPut(['message' => 'این بازی بر روی ' .$work_device. ' در حال انجام است.'], 409);

        $custom_game->delete();
        $this->__outPut(['message' => 'بازی با موفقیت حذف شد']);
    }

    public function list(Request $request)
    {
        $inputs = $request->all();
        $filter = [
            ['alias', 'like']
        ];
        $condition = $this->_filter($inputs, $filter);
        $devices = $this->gnet->customGame()->where($condition)->paginate(30);
        $this->__outPut($devices);
    }

    public function get(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'custom_game_id' => 'required|exists:'. App(CustomGame::class)->getTable() . ',id'
        ]);
        $customGame = CustomGame::find($inputs['custom_game_id']);
        $this->__outPut($customGame);
    }

}
