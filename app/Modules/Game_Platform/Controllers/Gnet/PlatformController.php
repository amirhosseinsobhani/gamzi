<?php


namespace App\Modules\Game_Platform\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Modules\Game_Platform\Models\GnetPlatformGame;
use App\Modules\Game_Platform\Models\Platform;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PlatformController extends GameziController
{
    public function list(Request $request)
    {
        $inputs = $request->all();
        $filter = [
            ['name', 'like'],
            ['alias', 'like'],
        ];
        $condition = $this->_filter($inputs, $filter);
        $platform = $this->gnet->platform()->where($condition)->get();
        $this->__outPut($platform);
    }

    public function get(Request $request)
    {
        $inputs = $request->all();
        $inputs['id'] = $inputs['gnet_platform_game_id'];
        $this->_checkValidation($inputs, [
            'id' => [
                'required',
                Rule::exists(App(GnetPlatformGame::class)->getTable())->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_platform_game_id']],
                        ['gnet_id', '=', $this->gnet->id],
                        ['parent_id', '=', null],
                        ['game_id', '=', null],
                    ]);
                })
            ]
        ]);

        $data['platform'] = GnetPlatformGame::find($inputs['gnet_platform_game_id'])->platform;
        $data['game_list'] = GnetPlatformGame::where('parent_id', $inputs['gnet_platform_game_id'])->with('game')->get()->pluck('game');
        $this->__outPut($data);
    }

    public function remove(Request $request)
    {
        $inputs = $request->all();

        $inputs['id'] = $inputs['gnet_platform_game_id'];
        $this->_checkValidation($inputs, [
            'id' => [
                'required',
                Rule::exists(App(GnetPlatformGame::class)->getTable())->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_platform_game_id']],
                        ['gnet_id', '=', $this->gnet->id],
                        ['parent_id', '=', null],
                        ['game_id', '=', null],
                    ]);
                })
            ]
        ]);

        GnetPlatformGame::find($inputs['gnet_platform_game_id'])->delete();
        $this->__outPut(['message' => 'دستگاه با موفقیت حذف شد']);
    }

    public function add(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'platform_id' => 'required|exists:' . App(Platform::class)->getTable() . ',id',
            'alias' => [
                'required',
                Rule::unique('gnet_platform_game')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['gnet_id', '=', $this->gnet->id],
                        ['alias', '=', $inputs['alias']]
                    ]);
                })
            ]
        ]);

        GnetPlatformGame::create($inputs);

        $this->__outPut(['message' => 'پلتفرم ها با موفقیت افزوده شد']);
    }

}
