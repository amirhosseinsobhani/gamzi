<?php


namespace App\Modules\Game_Platform\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Modules\Client\Controllers\Gnet\GnetController;
use App\Modules\Game_Platform\Models\Game;
use App\Modules\Game_Platform\Models\GnetPlatformGame;
use App\Modules\History\Models\GnetWorkUser;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class GameController extends GameziController
{
    public function list(Request $request)
    {
        $inputs = $request->all();
        $gnet = GnetController::gnetOwner($inputs['gnet_id']);
        $filter = [
            ['name', 'like'],
            ['company_id', '=']
        ];
        $condition = $this->_filter($inputs, $filter);
        $devices = $gnet->device()->where($condition)->paginate(30);
        $this->__outPut($devices);
    }

    public function remove(Request $request)
    {
        $inputs = $request->all();
        $device = $this->deviceFind($this->gnet->id, $inputs['device_id']);
        $this->gnet->device()->detach($device->id);
        $this->__outPut(['message' => 'دستگاه با موفقیت حذف شد']);
    }

    public function add(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'games' => 'required|array',
            'games.*.game_id' => 'exists:' . App(Game::class)->getTable() . ',id',
        ]);

        $data = array_map(function ($item) {
            return [
                'gnet_id' => $this->gnet->id,
                'game_id' => $item['game_id'],
            ];
        }, $inputs['games']);

        GnetPlatformGame::insert($data);

        $this->__outPut(['message' => 'بازی ها با موفقیت افزوده شدند']);
    }

    public function addGameToPlatform(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'gnet_platform_id' => [
                'required',
                Rule::exists(App(GnetPlatformGame::class)->getTable(), 'id')->where(function ($query) {
                    $query->where([
                        ['gnet_id', (new self())->gnet->id],
                        ['parent_id', null],
                        ['platform_id', '!=', null],
                    ]);
                })],
            'gnet_game_id.*' => [
                Rule::exists(App(GnetPlatformGame::class)->getTable(), 'game_id')->where(function ($query) {
                    $query->where([
                        ['gnet_id', (new self())->gnet->id],
                        ['parent_id', null]
                    ]);
                })
            ],
        ]);
        //if device already added to gnet refuse request.
        $query = GnetPlatformGame::where('parent_id', $inputs['gnet_platform_id'])->whereIn('game_id', $inputs['gnet_game_id'])->count();
        if ($query > 0)
            $this->__outPut(['message' => 'بازی مورد نظر قبلا به این دستگاه افزوده شده است'], 400);
        //if device already added to gnet refuse request.

        $data = array_map(function ($item) use ($inputs) {
            return [
                'gnet_id' => $this->gnet->id,
                'game_id' => $item,
                'parent_id' => $inputs['gnet_platform_id']
            ];
        }, $inputs['gnet_game_id']);

        GnetPlatformGame::insert($data);

        $this->__outPut(['message' => 'بازی ها با موفقیت افزوده شدند']);
    }

    public function platformGameList(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'gnet_platform_id' => [
                'required',
                Rule::exists(App(GnetPlatformGame::class)->getTable(), 'id')->where(function ($query) {
                    $query->where([
                        ['gnet_id', (new self())->gnet->id],
                        ['parent_id', null],
                        ['platform_id', '!=', null],
                    ]);
                })
            ],
        ]);

        $data['platform'] = GnetPlatformGame::find($inputs['gnet_platform_id'])->load('platform');
        $data['game'] = GnetPlatformGame::where('parent_id', $inputs['gnet_platform_id'])->with('game')->get();

        $this->__outPut($data);
    }

    public function removeGameFromPlatform(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'gnet_game_id' => [
                Rule::exists(App(GnetPlatformGame::class)->getTable(), 'id')->where(function ($query) {
                    $query->where([
                        ['gnet_id', (new self())->gnet->id],
                        ['parent_id', '!=',null],
                        ['platform_id', null],
                    ]);
                })
            ],
        ]);

        $online_work_platform = GnetWorkUser::where([
            ['gnet_platform_game_id', $inputs['gnet_game_id']],
            ['flg_online', GnetWorkUser::FlgOnline]
        ])->count();

        if ($online_work_platform > 0)
            $this->__outPut(['message' => 'بازی مورد نظر درحال استفاده می باشد و قابلیت حذف ندارد.'], 400);

        GnetPlatformGame::find($inputs['gnet_game_id'])->delete();
        $this->__outPut(['message' => 'بازی مورد نظر با موفقیت حذف شد']);
    }


}
