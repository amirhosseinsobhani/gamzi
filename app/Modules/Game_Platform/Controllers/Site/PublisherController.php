<?php


namespace App\Modules\Game_Platform\Controllers\Site;


use App\Http\Controllers\GameziController;
use App\Modules\Game_Platform\Models\Publisher;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PublisherController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }

    public function insert(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'alias' => 'required|unique:'.App(Publisher::class)->getTable().',alias',
            'name' => 'string',
            'logo' => 'file|mimes:jpg,jpeg,png|max:3072',
            'description' => 'string|max:500',
            'established_date' => 'date_format:Y-m'
        ]);

        if (isset($inputs['logo']))
            $inputs['logo'] = $this->_uploadFile($request->file('logo'), 'Publisher');

        Publisher::create($inputs);

        $this->__outPut(['message' => 'ناشر با موفقیت ثبت شد']);
    }

    public function get(Publisher $publisher)
    {
        $this->__outPut($publisher);
    }

    public function update(Publisher $publisher, Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'alias' => [
                'required',
                Rule::unique(App(Publisher::class)->getTable())->where(function ($query) use ($publisher) {
                    $query->where('id', '<>', $publisher->id);
                })
            ],
            'name' => 'string',
            'logo' => 'file|mimes:jpg,jpeg,png|max:3072',
            'description' => 'string|max:500',
            'established_date' => 'date_format:Y-m'
        ]);

        if (isset($inputs['logo']))
            $inputs['logo'] = $this->_uploadFile($request->file('logo'), 'Publisher');

        $publisher->update($inputs);

        $this->__outPut(['message' => 'ناشر با موفقیت بروز شد']);
    }

    public function list(Request $request)
    {
        $inputs = $request->all();
        $filter = [
            ['alias', 'like'],
            ['name', 'like'],
        ];
        $condition = $this->_filter($inputs, $filter);
        $devices = Publisher::where($condition)->paginate(20);
        $this->__outPut($devices);
    }

    public function delete(Publisher $publisher)
    {
        if (count($publisher->game) > 0)
            $this->__outPut(['message' => 'این ناشر به بازی اختصاص داده شده است و قابلیت حذف ندارد'], 409);

        $publisher->delete();

        $this->__outPut(['message' => 'ناشر با موفقیت حذف شد']);
    }

}
