<?php


namespace App\Modules\Game_Platform\Controllers\Site;


use App\Http\Controllers\GameziController;
use App\Modules\Game_Platform\Models\Developer;
use Illuminate\Http\Request;

class DeveloperController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }

    public function insert(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'alias' => 'required|string|min:3|unique:' . App(Developer::class)->getTable() . ',alias',
            'name' => 'string',
            'description' => 'string|max:500',
            'logo' => 'file|mimes:jpg,jpeg,png|max:3072',
            'established_date' => 'date_format:Y-m'
        ]);

        if (isset($inputs['logo']))
            $inputs['logo'] = $this->_uploadFile($request->file('logo'), 'Developer');

        Developer::create($inputs);

        $this->__outPut(['message' => 'توسعه دهنده با موفقیت ثبت شد']);
    }

    public function get(Developer $developer)
    {
        $this->__outPut($developer);
    }

    public function update(Developer $developer, Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'alias' => 'required|string|min:3',
            'name' => 'string',
            'logo' => 'file|mimes:jpg,jpeg,png|max:3072',
            'established_date' => 'date_format:Y-m'
        ]);

        if (isset($inputs['logo']))
            $inputs['logo'] = $this->_uploadFile($request->file('logo'), 'Developer');

        $developer->update($inputs);
        $this->__outPut(['message' => 'توسعه دهنده با موفقیت بروز شد']);
    }

    public function list(Request $request)
    {
        $inputs = $request->all();
        $filter = [
            ['alias', 'like'],
            ['name', 'like'],
        ];
        $condition = $this->_filter($inputs, $filter);
        $developer = Developer::where($condition)->paginate(20);
        $this->__outPut($developer);
    }

    public function delete(Developer $developer)
    {
        if (count($developer->game) > 0)
            $this->__outPut(['message' => 'توسعه دهنده به یک بازی اختصاص داده شده است و قابلیت حذف ندارد.'], 409);

        $developer->delete();
        $this->__outPut(['message' => 'توسعه دهنده با موفقیت حذف شد']);
    }

}
