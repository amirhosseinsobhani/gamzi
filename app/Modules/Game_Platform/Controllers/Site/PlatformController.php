<?php


namespace App\Modules\Game_Platform\Controllers\Site;


use App\Http\Controllers\GameziController;
use App\Modules\Game_Platform\Models\Platform;
use App\Modules\Game_Platform\Models\PlatformCategory;
use Illuminate\Http\Request;

class PlatformController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return void
     */
    public function insert(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'alias' => 'required|unique:' . App(Platform::class)->getTable() . ',alias',
            'platform_category_id' => 'required|exists:' . App(PlatformCategory::class)->getTable() . ',id',
            'name' => 'required|string',
            'description' => 'string',
            'image' => 'file|mimes:png,jpeg,jpeg|max:3072',
            'release_time' => 'date_format:Y-m-d'
        ]);

        if (isset($inputs['image']))
            $inputs['image'] = $this->_uploadFile($request->file('image'), 'Platform');

        Platform::create($inputs);
        $this->__outPut(['message' => 'پلتفرم با موفقیت ثبت شد']);
    }

    /**
     * @param Platform $platform
     * @return void $device
     */
    public function get(Platform $platform)
    {
        $this->__outPut($platform);
    }
    /**
     * @param Request $request
     * @return void devices
     */
    public function list(Request $request)
    {
        $inputs = $request->all();
        $filter = [
            ['alias', 'like'],
            ['name', 'like'],
            ['platform_category_id', '='],
        ];
        $condition = $this->_filter($inputs, $filter);
        $devices = Platform::where($condition)->paginate(20);
        $this->__outPut($devices);
    }

    /**
     * @param Platform $platform
     * @return void message
     * @throws \Exception
     */
    public function delete(Platform $platform)
    {
        if (count($platform->gnet) > 0)
            $this->__outPut(['message' => 'این پلتفرم به گیمنت اختصاص داده شده است و قابلیت حذف ندارد'], 409);

        if (count($platform->game) > 0)
            $this->__outPut(['message' => 'این پلتفرم به بازی اختصاص داده شده است و قابلیت حذف ندارد'], 409);

        $platform->delete();
        $this->__outPut(['message' => 'حذف با موفقیت انجام شد']);
    }

    /**
     * @param Platform $platform
     * @param Request $request
     * @return void message
     */
    public function update(Platform $platform, Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'platform_category_id' => 'required|exists:' . App(PlatformCategory::class)->getTable() . ',id',
            'alias' => 'string',
            'description' => 'string',
            'image' => 'file|mimes:png,jpeg,jpeg|max:3072'
        ]);
        if (isset($inputs['image']))
            $inputs['image'] = $this->_uploadFile($request->file('image'), 'Platform');

        $platform->update($inputs);
        $this->__outPut(['message' => 'بروزرسانی با موفقیت انجام شد']);
    }
}
