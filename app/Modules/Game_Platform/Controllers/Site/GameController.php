<?php


namespace App\Modules\Game_Platform\Controllers\Site;


use App\Http\Controllers\GameziController;
use App\Modules\Game_Platform\Models\Developer;
use App\Modules\Game_Platform\Models\Game;
use App\Modules\Game_Platform\Models\Genre;
use App\Modules\Game_Platform\Models\Publisher;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class GameController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return void message
     */
    public function insert(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'developer_id' => 'required|exists:' . App(Developer::class)->getTable() . ',id',
            'publisher_id' => 'required|exists:' . App(Publisher::class)->getTable() . ',id',
            'genre_id' => 'required|exists:' . App(Genre::class)->getTable() . ',id',
            'alias' => 'required|string|unique:' . App(Game::class)->getTable() . ',alias',
            'esrb_rating_id' => 'required|exists:esrb_rating,id',
            'name' => 'string',
            'description' => 'string',
            'description_en' => 'string',
            'metacritic' => 'numeric',
            'metacritic_url' => 'string',
            'image' => 'file|mimes:png,jpeg,jpeg|max:3072',
            'background_image' => 'file|mimes:png,jpeg,jpeg|max:3072',
            'rating' => 'numeric',
            'rating_top' => 'numeric',
            'release_time' => 'date_format:Y-m-d',
            'playtime' => 'numeric',
            'website' => 'string|url',
            'tags' => 'string'
        ]);

        if (isset($inputs['image']))
            $inputs['image'] = $this->_uploadFile($request->file('image'), 'Game');

        if (isset($inputs['background_image']))
            $inputs['background_image'] = $this->_uploadFile($request->file('background_image'), 'Game');

        $game = Game::create($inputs);
        $game->developer()->sync($inputs['developer_id']);
        $game->publisher()->sync($inputs['publisher_id']);
        $game->genre()->sync($inputs['genre_id']);

        $this->__outPut(['message' => 'بازی با موفقیت درج شد']);
    }

    /**
     * @param Game $game
     * @return void message
     */
    public function get(Game $game)
    {
        $this->__outPut($game->load('developer', 'publisher', 'genre', 'esrbRating'));
    }

    /**
     * @param Game $game
     * @param Request $request
     * @return void message
     */
    public function update(Game $game, Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'developer_id' => 'required|exists:' . App(Developer::class)->getTable() . ',id',
            'publisher_id' => 'required|exists:' . App(Publisher::class)->getTable() . ',id',
            'genre_id' => 'required|exists:' . App(Genre::class)->getTable() . ',id',
            'alias' => [
                'required',
                'string',
                Rule::unique(App(Game::class)->getTable(), 'alias')->where(function ($query) use ($game) {
                    $query->where('id', '<>', $game->id);
                })
            ],
            'esrb_rating_id' => 'required|exists:esrb_rating,id',
            'name' => 'string',
            'description' => 'string',
            'description_en' => 'string',
            'metacritic' => 'numeric',
            'metacritic_url' => 'string',
            'image' => 'file|mimes:png,jpeg,jpeg|max:3072',
            'background_image' => 'file|mimes:png,jpeg,jpeg|max:3072',
            'rating' => 'numeric',
            'rating_top' => 'numeric',
            'release_time' => 'date_format:Y-m-d',
            'playtime' => 'numeric',
            'website' => 'string|url',
            'tags' => 'string'
        ]);

        if (isset($inputs['image']))
            $inputs['image'] = $this->_uploadFile($request->file('image'), 'Game');

        if (isset($inputs['background_image']))
            $inputs['background_image'] = $this->_uploadFile($request->file('background_image'), 'Game');

        $game->update($inputs);
        $game->developer()->sync($inputs['developer_id']);
        $game->publisher()->sync($inputs['publisher_id']);
        $game->genre()->sync($inputs['genre_id']);

        $this->__outPut(['message' => 'بازی با موفقیت بروز شد']);
    }

    /**
     * @param Game $game
     * @return void message
     * @throws \Exception
     */
    public function delete(Game $game)
    {
        if (!empty($game->gnet))
            $this->__outPut(['message' => 'بازی به گیمنتی اختصاص داده شده است و قابلیت حذف ندارد'], 409);

        $game->delete();
        $this->__outPut(['message' => 'بازی با موفقیت حذف شد']);
    }

    /**
     * @param Request $request
     * @return void message
     */
    public function list(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'publisher_id' => 'exists:' . App(Publisher::class)->getTable() . ',id',
            'genre_id' => 'exists:' . App(Genre::class)->getTable() . ',id',
            'developer_id' => 'exists:' . App(Developer::class)->getTable() . ',id',
            'release_start_time' => 'date_format:Y-m-d',
            'release_end_time' => 'date_format:Y-m-d',
        ]);

        $filter = [
            ['publisher_id', '='],
            ['genre_id', '='],
            ['developer_id', '='],
            ['alias', 'like'],
            ['name', 'like'],
            ['release_start_time', '>=', 'release_time'],
            ['release_end_time', '<=', 'release_time'],
        ];
        $condition = $this->_filter($inputs, $filter);
        $games = Game::where($condition)->paginate(20);
        $this->__outPut($games);
    }
}
