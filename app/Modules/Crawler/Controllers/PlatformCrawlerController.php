<?php


namespace App\Modules\Crawler\Controllers;


use App\Http\Controllers\GameziController;
use App\Modules\Game_Platform\Models\Platform;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Throwable;

class PlatformCrawlerController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }

    public function add($page = 1)
    {
        $response = Http::get('https://api.rawg.io/api/platforms?page=' . $page);

        $platforms = $response->json();

        $data = array_map(function ($item) {
            return [
                'id' => $item['id'],
                'alias' => $item['name'],
                'release_time' => $item['year_start'],
            ];
        }, $platforms['results']);

        foreach ($data as $platform) {
            try {
                $validation = Validator::make($platform, [
//                    'id' => 'required|unique:' . App(Genre::class)->getTable() . ',id',
                    'alias' => 'required|unique:' . App(Platform::class)->getTable() . ',alias',
                ]);
                if (!$validation->fails()) {
                    DB::table(App(Platform::class)->getTable())->insert($platform);
                }
            } catch (Throwable $throwable) {

            }
        }
        $this->__outPut(['done!']);

    }

}
