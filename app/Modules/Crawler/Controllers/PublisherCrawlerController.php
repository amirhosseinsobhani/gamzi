<?php


namespace App\Modules\Crawler\Controllers;


use App\Http\Controllers\GameziController;
use App\Modules\Game_Platform\Models\Publisher;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Throwable;

class PublisherCrawlerController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }


    public function add($minPage = 1, $maxPage = 1)
    {
        foreach (range($minPage, $maxPage) as $page) {

            $response = Http::get('https://api.rawg.io/api/publishers?page=' . $page);

            $publishers = $response->json();

            $data = array_map(function ($item) {
                return [
                    'id' => $item['id'],
                    'alias' => $item['name'],
                ];
            }, $publishers['results']);

            foreach ($data as $publisher) {
                try {
//                    $validation = Validator::make($developer, [
////                    'id' => 'required|unique:' . App(Genre::class)->getTable() . ',id',
//                        'alias' => 'required|unique:' . App(Publisher::class)->getTable() . ',alias',
//                    ]);
//                    if (!$validation->fails()) {
                        DB::table(App(Publisher::class)->getTable())->insert($publisher);
//                    }
                } catch (Throwable $throwable) {
                    Log::info($throwable);

                }
            }

            echo 'page number = ' . $page . PHP_EOL;
        }

    }
}
