<?php


namespace App\Modules\Crawler\Controllers;


use App\Http\Controllers\GameziController;
use App\Modules\Game_Platform\Models\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Throwable;

class GenreCrawlerController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }

    public function add(Request $request)
    {
        $inputs = $request->all();
        $response = Http::get('https://api.rawg.io/api/genres');

        $genres = $response->json();

        $data = array_map(function ($item) {
            return [
                'id' => $item['id'],
                'title' => $item['name']
            ];
        }, $genres['results']);

        foreach ($data as $genre) {
            try {
                $validation = Validator::make($genre, [
//                    'id' => 'required|unique:' . App(Genre::class)->getTable() . ',id',
                    'title' => 'required|unique:' . App(Genre::class)->getTable() . ',title',
                ]);
                if (!$validation->fails()) {
                    DB::table(App(Genre::class)->getTable())->insert($genre);
                }
            } catch (Throwable $throwable) {

            }
        }
        $this->__outPut(['done!']);
    }

}
