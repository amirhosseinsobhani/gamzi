<?php


namespace App\Modules\Crawler\Controllers;


use App\Http\Controllers\GameziController;
use App\Modules\Game_Platform\Models\Developer;
use App\Modules\Game_Platform\Models\Game;
use App\Modules\Game_Platform\Models\Media;
use App\Modules\Game_Platform\Models\Publisher;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

class GameCrawlerController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }

    public function add($minPage = 1, $maxPage = 1)
    {
        foreach (range($minPage, $maxPage) as $page) {

            $response = Http::get('https://api.rawg.io/api/games?page_size=40&page=' . $page);

            $games = $response->json();

            foreach ($games['results'] as $game) {
                try {

                    $media_image = array_values(array_filter(array_map(function ($item) {
                        if ($item['id'] == -1) {
                            return [];
                        }
                        return [
                            'id' => $item['id'],
                            'type' => Media::ImageType,
                            'url' => $item['image']
                        ];
                    }, $game['short_screenshots'])));

                    $game_response = Http::get('https://api.rawg.io/api/games/' . $game['id']);
                    $game = $game_response->json();

                    $tags = array_map(function ($item) {
                        return $item['name'];
                    }, $game['tags']);

                    $tags = implode(',', $tags);

                    $game_data = [
                        'id' => $game['id'],
                        'esrb_rating_id' => $game['esrb_rating']['id'],
                        'alias' => $game['name'],
                        'name' => $game['name_original'],
        //            'description',
                        'description_en' => $game['description'],
                        'metacritic' => $game['metacritic'],
                        'metacritic_url' => $game['metacritic_url'],
                        'background_image' => $game['background_image'],
                        'rating' => $game['rating'],
                        'rating_top' => $game['rating_top'],
                        'playtime' => $game['playtime'],
                        'website' => $game['website'],
        //            'image',
                        'release_time' => $game['released'],
        //            'use_count',
                        'tags' => $tags
                    ];

                    $game_platform_data = array_map(function ($item) use ($game) {
                        return [
                            'game_id' => $game['id'],
                            'platform_id' => @$item['platform']['id']
                        ];
                    }, $game['platforms']);

                    $game_publisher_data = array_map(function ($item) use ($game) {
                        try {
                            $pub = Publisher::find($item['id']);
                            if (empty($pub))
                                DB::table('publisher')->insert(['id' => $item['id'], 'alias' => $item['name']]);
                        } catch (Throwable $throwable) {
                            Log::info($throwable);
                        }

                        return [
                            'game_id' => $game['id'],
                            'publisher_id' => @$item['id']
                        ];
                    }, $game['publishers']);

                    $game_developer_data = array_map(function ($item) use ($game) {
                        try {
                            $dev = Developer::find($item['id']);
                            if (empty($dev))
                                DB::table('developer')->insert(['id' => $item['id'], 'alias' => $item['name']]);

                        } catch (Throwable $throwable) {
                            Log::info($throwable);
                        }

                        return [
                            'game_id' => $game['id'],
                            'developer_id' => @$item['id']
                        ];
                    }, $game['developers']);

                    $game_genre_data = array_map(function ($item) use ($game) {
                        return [
                            'game_id' => $game['id'],
                            'genre_id' => @$item['id']
                        ];
                    }, $game['genres']);

                    $media_clip = [
                        'type' => Media::VideoType,
                        'url' => $game['clip']['clip'],
                        'preview' => $game['clip']['preview']
                    ];

                    DB::table(App(Game::class)->getTable())->insert($game_data);
                    DB::table('game_developer')->insert($game_developer_data);
                    DB::table('game_genre')->insert($game_genre_data);
                    DB::table('game_platform')->insert($game_platform_data);
                    DB::table('game_publisher')->insert($game_publisher_data);

                    DB::table('media')->insert($media_image);

                    $media_clip = Media::create($media_clip);

                    DB::table('media_game')->insert([
                        'game_id' => $game['id'],
                        'media_id' => $media_clip->id
                    ]);

                    $media_image = array_map(function ($item) use ($game) {
                        return [
                            'media_id' => $item['id'],
                            'game_id' => $game['id']
                        ];
                    }, $media_image);

                    DB::table('media_game')->insert($media_image);


                } catch (Throwable $throwable) {
                    Log::info($throwable);
                }

            }
        }

    }
}
