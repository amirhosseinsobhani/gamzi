<?php

use Illuminate\Support\Facades\Route;

$module_namespace = "App\Modules\Crawler\Controllers";

Route::prefix('api')->middleware('api')->namespace($module_namespace)->group(function () {
    Route::prefix('site/crawler')->group(function () {
        Route::post('genre', 'GenreCrawlerController@add');
        Route::post('platform/{page?}', 'PlatformCrawlerController@add');
        Route::post('developer/{minPage?}/{maxPage?}', 'DeveloperCrawlerController@add');
        Route::post('publisher/{minPage?}/{maxPage?}', 'PublisherCrawlerController@add');
        Route::post('game/{minPage?}/{maxPage?}', 'GameCrawlerController@add');
    });
});
