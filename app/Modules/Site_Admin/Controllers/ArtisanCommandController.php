<?php

namespace App\Modules\Site_Admin\Controllers;

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\GameziController;


class ArtisanCommandController extends GameziController
{
    public function __construct()
    {
        $this->Guard = false;
        $this->gnet_id_required = false;
        parent::__construct();
    }

    public function runJob()
    {
//        Artisan::call('queue:work --stop-when-empty ');
        Artisan::call('migrate');
        return response('Jobs Finished');
    }
}
