<?php

use Illuminate\Support\Facades\Route;

$module_namespace = "App\Modules\Site_Admin\Controllers";

Route::prefix('api')->middleware('api')->namespace($module_namespace)->group(function () {
    Route::prefix('site')->group(function () {
        Route::prefix('admin')->group(function () {
            Route::get('run-job', 'ArtisanCommandController@runJob');
        });
    });
});
