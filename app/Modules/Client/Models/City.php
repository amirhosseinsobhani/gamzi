<?php


namespace App\Modules\Client\Models;


use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'province_city';

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

}
