<?php


namespace App\Modules\Client\Models;


use App\Modules\Game_Platform\Models\GnetPlatformGame;
use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    protected $table = 'gnet_price';

    protected $fillable = [
        'gnet_id',
        'gnet_platform_game_id',
        'number_person',
        'price',
        'pro_price'
    ];

    public function platform()
    {
        return $this->belongsTo(GnetPlatformGame::class, 'gnet_platform_game_id');
    }

    public function gnet()
    {
        return $this->belongsTo(Gnet::class, 'gnet_id');
    }
}
