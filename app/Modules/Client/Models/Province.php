<?php


namespace App\Modules\Client\Models;


use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'province';

    public function city()
    {
        return $this->hasMany(City::class);
    }
}
