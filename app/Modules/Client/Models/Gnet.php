<?php


namespace App\Modules\Client\Models;


use App\Modules\Buffet\Models\Food;
use App\Modules\Game_Platform\Models\CustomGame;
use App\Modules\Game_Platform\Models\Game;
use App\Modules\Game_Platform\Models\GnetPlatformGame;
use App\Modules\Game_Platform\Models\Platform;
use App\Modules\History\Models\GnetWorkTransaction;
use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Model;

class Gnet extends Model
{
    protected $table = 'gnet';

    protected $fillable = [
        'user_id',
        'province_city_id',
        'name',
        'phone',
        'email',
        'site',
        'address',
        'work_start_time',
        'work_end_time',
        'flg_active'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function platform()
    {
        return $this->belongsToMany(Platform::class, 'gnet_platform_game', 'gnet_id', 'platform_id')->withPivot('game_id', 'alias');
    }

    public function game()
    {
        return $this->belongsToMany(Game::class, 'gnet_device_game', 'gnet_id', 'game_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function food()
    {
        return $this->belongsToMany(Food::class, 'gnet_food', 'gnet_id')
            ->withPivot('price', 'count', 'status');
    }

    public function customGame()
    {
        return $this->hasMany(CustomGame::class, 'gnet_id');
    }

    public function price()
    {
        return $this->hasMany(Pricing::class);
    }

    public function member()
    {
        return $this->belongsToMany(User::class, 'gnet_membership', 'gnet_id', 'user_id');
    }

    public function workTransaction()
    {
        return $this->hasMany(GnetWorkTransaction::class, 'gnet_id');
    }

    public function gnetPlatform()
    {
        return $this->hasMany(GnetPlatformGame::class, 'gnet_id')->where([
            ['parent_id', '=', null],
            ['game_id', '=', null]
        ]);
    }

}
