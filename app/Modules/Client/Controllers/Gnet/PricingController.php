<?php


namespace App\Modules\Client\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Modules\Client\Models\Pricing;
use App\Modules\Game_Platform\Models\GnetPlatformGame;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PricingController extends GameziController
{
    public function list()
    {
        $prices = $this->gnet->price()->with('platform')->get();
        $this->__outPut($prices);
    }

    public function add(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'gnet_platform_game_id' => 'required|exists:' . App(GnetPlatformGame::class)->getTable() . ',id|unique:'.App(Pricing::class)->getTable().',gnet_platform_game_id',
            'number_person' => 'required|numeric|min:1,max:4',
            'price' => 'required|numeric|max:10000000000',
            'pro_price' => 'required|numeric|max:10000000000'
        ]);
        $this->gnet->price()->create($inputs);
        $this->__outPut(['message' => 'تعرفه با موفقیت ثبت شد']);
    }

    public function remove(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'gnet_price_id' => 'required|exists:' . App(Pricing::class)->getTable() . ',id'
        ]);
        Pricing::find($inputs['gnet_price_id'])->delete();
        $this->__outPut(['message' => 'تعرفه با موفقیت حذف شد']);
    }

    public function calculateAmount(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'gnet_platform_game_id' => [
                'required',
                Rule::exists(App(GnetPlatformGame::class)->getTable(), 'id')->where(function ($query) {
                    $query->where([
                        ['gnet_id', (new self())->gnet->id],
                        ['parent_id', null],
                        ['platform_id', '!=', null],
                    ]);
                })],
            'number_person' => 'required|numeric|min:1|max:4',
            'start_time' => 'required|date_format:Y-m-d H:i',
            'end_time' => 'required|date_format:Y-m-d H:i'
        ]);

        $start_time = Carbon::parse($inputs['start_time']);
        $end_time = Carbon::parse($inputs['end_time']);
        $time_interval = $start_time->diff($end_time);

        if ($end_time < $start_time)
            $this->__outPut(['message' => 'زمان انتها نمی تواند از زمان شروع کوچکتر باشد'], 400);

        $price_record = Pricing::where([
            ['gnet_id', $this->gnet->id],
            ['gnet_platform_game_id', $inputs['gnet_platform_game_id']],
            ['number_person', $inputs['number_person']]
        ])->first();

        if (empty($price_record))
            $this->__outPut(['message' => 'تعرفه ای برای این تعداد کاربر و این دستگاه ثبت نشده است'], 400);

        $amount = $price_record->price * ($time_interval->d * 24 + $time_interval->format('%h.%i'));
        $this->__outPut(['amount' => $amount]);
    }

    public function calculateTime(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'gnet_platform_game_id' => [
                'required',
                Rule::exists(App(GnetPlatformGame::class)->getTable(), 'id')->where(function ($query) {
                    $query->where([
                        ['gnet_id', (new self())->gnet->id],
                        ['parent_id', null],
                        ['platform_id', '!=', null],
                    ]);
                })],
            'number_person' => 'required|numeric|min:1|max:4',
            'amount' => 'required|numeric|min:100|max:100000000'
        ]);

        $price_record = Pricing::where([
            ['gnet_id', $this->gnet->id],
            ['gnet_platform_game_id', $inputs['gnet_platform_game_id']],
            ['number_person', $inputs['number_person']]
        ])->first();

        if (empty($price_record))
            $this->__outPut(['message' => 'تعرفه ای برای این تعداد کاربر و این دستگاه ثبت نشده است'], 400);

        $time = round($inputs['amount'] / $price_record->price, 2);

        $start_time = Carbon::now();
        $end_time = Carbon::now()->addHour(round($time))->addMinute($time - round($time))->format('Y-m-d H:i');

        $data = [
            'start_time' => $start_time->format('Y-m-d H:i'),
            'end_time' => $end_time
        ];

        $this->__outPut($data);
    }
}
