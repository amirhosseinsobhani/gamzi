<?php


namespace App\Modules\Client\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Http\Traits\Who;
use App\Modules\Client\Models\City;
use App\Modules\Client\Models\Gnet;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class GnetController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }

    public function insert(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'province_city_id' => 'required|exists:' . App(City::class)->getTable() . ',id',
            'name' => 'required|string|min:3',
            'phone' => 'numeric',
            'email' => 'email:dns,rfc',
            'site' => 'url',
            'address' => 'string',
            'image' => 'file|mimes:jpg,jpeg,png|max:3072',
            'work_start_time' => 'required|date_format:H:i',
            'work_end_time' => 'required|date_format:H:i',
        ]);

        if (isset($inputs['image']))
            $inputs['image'] = $this->_uploadFile($request->file('image'), 'Gnet-Logo');

        $this->user->gnet()->create($inputs);
        $this->__outPut(['message' => 'گیمنت با موفقیت ثبت شد']);
    }

    public function get(Gnet $gnet)
    {
        Who::checkOwner($gnet);
        $this->__outPut($gnet);
    }

    public function update(Gnet $gnet, Request $request)
    {
        Who::checkOwner($gnet);
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'province_city_id' => 'exists:' . App(City::class)->getTable() . ',id',
            'name' => 'string|min:3',
            'phone' => 'numeric',
            'email' => 'email:dns,rfc',
            'site' => 'url',
            'address' => 'string',
            'image' => 'file|mimes:jpg,jpeg,png|max:3072',
            'work_start_time' => 'required|date_format:H:i',
            'work_end_time' => 'required|date_format:H:i',
        ]);

        if (isset($inputs['image']))
            $inputs['image'] = $this->_uploadFile($request->file('image'), 'Gnet-Logo');

        $gnet->update($inputs);
        $this->__outPut(['message' => 'گیمنت با موفقیت بروز شد']);
    }

}
