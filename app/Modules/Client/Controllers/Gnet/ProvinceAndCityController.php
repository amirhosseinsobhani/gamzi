<?php


namespace App\Modules\Client\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Modules\Client\Models\Province;

class ProvinceAndCityController extends GameziController
{
    public function __construct()
    {
        $this->Guard = false;
        $this->gnet_id_required = false;
        parent::__construct();
    }

    public function index()
    {
        $province_city = Province::with('city')->get();
        $this->__outPut($province_city);
    }

}
