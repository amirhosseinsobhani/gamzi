<?php

use Illuminate\Support\Facades\Route;

$module_namespace = "App\Modules\Client\Controllers";

Route::prefix('api')->middleware('api')->namespace($module_namespace)->group(function () {
//    Route::prefix('site')->group(function () {
//        Route::prefix('device')->group(function () {
//        });
//    });

    Route::prefix('client')->group(function () {
        Route::prefix('gnet')->group(function () {
            Route::post('/', 'Gnet\GnetController@insert');
            Route::post('update/{gnet}', 'Gnet\GnetController@update');
            Route::get('{gnet}', 'Gnet\GnetController@get');
        });
        Route::prefix('province-city')->group(function () {
            Route::get('/', 'Gnet\ProvinceAndCityController@index');
        });
        Route::prefix('price')->group(function () {
            Route::post('list', 'Gnet\PricingController@list');
            Route::post('/', 'Gnet\PricingController@add');
            Route::post('remove', 'Gnet\PricingController@remove');
            Route::post('calculate-amount', 'Gnet\PricingController@calculateAmount');
            Route::post('calculate-time', 'Gnet\PricingController@calculateTime');
        });
    });
});
