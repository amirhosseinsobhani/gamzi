<?php

use Illuminate\Support\Facades\Route;

$module_namespace = "App\Modules\Dashboard\Controllers";

Route::prefix('api/client/dashboard')->middleware('api')->namespace($module_namespace)->group(function () {
    Route::prefix('game-time')->group(function () {
        Route::post('/', 'Gnet\GameTimeController@add');
        Route::post('edit', 'Gnet\GameTimeController@update');
        Route::post('delete', 'Gnet\GameTimeController@remove');
        Route::post('list', 'Gnet\GameTimeController@list');

        Route::prefix('food')->group(function () {
            Route::post('/', 'Gnet\FoodController@addFoodToGameTime');
            Route::post('remove', 'Gnet\FoodController@removeFoodGameTime');
        });
    });

    Route::prefix('platform')->group(function () {
        Route::post('list', 'Gnet\DeviceController@platformList');
    });


});
