<?php


namespace App\Modules\Dashboard\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Modules\Buffet\Models\CustomFood;
use App\Modules\Buffet\Models\GnetFood;
use App\Modules\History\Models\GnetFoodTransaction;
use App\Modules\History\Models\GnetWorkTransaction;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class FoodController extends GameziController
{
    public function addFoodToGameTime(Request $request)
    {
        $inputs = $request->all();

        $this->_checkValidation($inputs, [
            'gnet_work_transaction_id' => [
                'required',
                Rule::exists(App(GnetWorkTransaction::class)->getTable(), 'id')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_work_transaction_id']],
                        ['status', '=', GnetWorkTransaction::online],
                    ]);
                }),
                Rule::exists('gnet_work_transaction_platform')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['gnet_id', '=', $this->gnet->id],
                        ['gnet_work_transaction_id', '=', $inputs['gnet_work_transaction_id']],
                    ]);
                })
            ],
            'gnet_food_id' => [
                'required_without:gnet_custom_food_id',
                'nullable',
                Rule::exists(App(GnetFood::class)->getTable(), 'id')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_food_id']],
                        ['gnet_id', '=', $this->gnet->id]
                    ]);
                })
            ],
            'gnet_custom_food_id' => [
                'required_without:gnet_food_id',
                'nullable',
                Rule::exists(App(CustomFood::class)->getTable(), 'id')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_custom_food_id']],
                        ['gnet_id', '=', $this->gnet->id]
                    ]);
                })
            ],
            'amount' => 'required|numeric'
        ]);

        GnetFoodTransaction::create($inputs);

        $this->__outPut(['message' => 'خوراکی با موفقیت افزوده شد']);

    }

    public function removeFoodGameTime(Request $request)
    {
        $inputs = $request->all();

        $this->_checkValidation($inputs, [
            'gnet_work_transaction_id' => [
                'required',
                Rule::exists(App(GnetWorkTransaction::class)->getTable(), 'id')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_work_transaction_id']],
                    ]);
                }),
                Rule::exists('gnet_work_transaction_platform')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['gnet_id', '=', $this->gnet->id],
                        ['gnet_work_transaction_id', '=', $inputs['gnet_work_transaction_id']],
                    ]);
                })
            ],
            'gnet_food_transaction_id' => [
                'required',
                'nullable',
                Rule::exists(App(GnetFoodTransaction::class)->getTable(), 'id')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_food_transaction_id']],
                        ['gnet_work_transaction_id', '=', $inputs['gnet_work_transaction_id']],
                    ]);
                }),
            ]
        ]);

        GnetFoodTransaction::find($inputs['gnet_food_transaction_id'])->delete();

        $this->__outPut(['message' => 'خوراکی با موفقیت حذف شد']);
    }

}
