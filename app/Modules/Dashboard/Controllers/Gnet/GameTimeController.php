<?php


namespace App\Modules\Dashboard\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Modules\Client\Models\Pricing;
use App\Modules\Game_Platform\Models\CustomGame;
use App\Modules\Game_Platform\Models\GnetPlatformGame;
use App\Modules\History\Models\GnetWorkTransaction;
use App\Modules\History\Models\GnetWorkUser;
use App\Modules\User\Models\MemberShip;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class GameTimeController extends GameziController
{
    public function add(Request $request)
    {
        $inputs = $request->all();

        $this->openingTimeValidation($inputs);

        $online_work = DB::table('gnet_work_transaction_platform')->where([
            'gnet_id' => $this->gnet->id,
            'gnet_platform_id' => $inputs['gnet_platform_id'],
        ])->count();

        if ($online_work > 0)
            $this->__outPut(['message' => 'برای این دستگاه یک زمان آنلاین وجود دارد'], 400);

        $this->createTransactionRecord($inputs);

        $this->__outPut(['message' => 'زمان برای این دستگاه با موفقیت ثبت شد']);

    }

    public function update(Request $request)
    {
        $inputs = $request->all();

        $this->openingTimeValidation($inputs);

        $this->_checkValidation($inputs, [
            'gnet_work_transaction_id' => [
                'required',
                Rule::exists('gnet_work_transaction_platform')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['gnet_id', '=', $this->gnet->id],
                        ['gnet_platform_id', '=', $inputs['gnet_platform_id']],
                        ['gnet_work_transaction_id', '=', $inputs['gnet_work_transaction_id']],
                    ]);
                }),
                Rule::exists(App(GnetWorkTransaction::class)->getTable(), 'id')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_work_transaction_id']],
                        ['status', '=', GnetWorkTransaction::online],
                    ]);
                })
            ]
        ]);

        GnetWorkTransaction::find($inputs['gnet_work_transaction_id'])->update([
            'status' => GnetWorkTransaction::offline
        ]);

        $this->createTransactionRecord($inputs);

        $this->__outPut(['message' => 'زمان برای این دستگاه با موفقیت بروز شد']);

    }

    private function createTransactionRecord($inputs)
    {
        $inputs['flg_free_end_time'] == 0 ? $end_time = $inputs['end_time'] : $end_time = null;

        $gnet_work_transaction = GnetWorkTransaction::create([
            'gnet_game_id' => $inputs['gnet_game_id'],
            'gnet_custom_game_id' => $inputs['gnet_custom_game_id'],
            'total_amount' => $inputs['amount'],
            'start_time' => $inputs['start_time'],
            'end_time' => $end_time,
        ]);

        DB::table('gnet_work_transaction_platform')->insert([
            'gnet_id' => $this->gnet->id,
            'gnet_work_transaction_id' => $gnet_work_transaction->id,
            'gnet_platform_id' => $inputs['gnet_platform_id'],
        ]);

        $work_user_data = array_map(function ($item) use ($end_time, $inputs, $gnet_work_transaction) {
            return [
                'gnet_work_transaction_id' => $gnet_work_transaction->id,
                'user_id' => $item,
                'start_time' => $inputs['start_time'],
                'end_time' => $end_time
            ];
        }, $inputs['user_id']);

        DB::table(App(GnetWorkUser::class)->getTable())->insert($work_user_data);
    }

    private function openingTimeValidation($inputs)
    {
        $this->_checkValidation($inputs, [
            "user_id" => [
                'required',
                Rule::exists(App(MemberShip::class)->getTable())->where(function ($query) use ($inputs) {
                    $query->where('gnet_id', $this->gnet->id)->whereIn('user_id', $inputs['user_id']);
                })
            ],
            'gnet_platform_id' => [
                'required',
                Rule::exists(App(GnetPlatformGame::class)->getTable(), 'id')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_platform_id']],
                        ['gnet_id', '=', $this->gnet->id],
                        ['parent_id', '=', null],
                        ['game_id', '=', null],
                    ]);
                })
            ],
            'gnet_game_id' => [
                'required_without:gnet_custom_game_id',
                'nullable',
                Rule::exists(App(GnetPlatformGame::class)->getTable(), 'id')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_game_id']],
                        ['gnet_id', '=', $this->gnet->id],
                        ['parent_id', '=', null],
                        ['platform_id', '=', null],
                    ]);
                })
            ],
            'gnet_custom_game_id' => [
                'required_without:gnet_game_id',
                'nullable',
                Rule::exists(App(CustomGame::class)->getTable(), 'id')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_custom_game_id']],
                        ['gnet_id', '=', $this->gnet->id]
                    ]);
                })
            ],
            'start_time' => 'required|date_format:Y-m-d H:i',
            'end_time' => 'required|date_format:Y-m-d H:i',
            'flg_free_end_time' => 'required|boolean',
            'number_person' => 'required|numeric',
        ]);

        $start_time = Carbon::parse($inputs['start_time']);
        $end_time = Carbon::parse($inputs['end_time']);
        $time_interval = $start_time->diff($end_time);

        if ($end_time < $start_time)
            $this->__outPut(['message' => 'زمان انتها نمی تواند از زمان شروع کوچکتر باشد'], 400);

        $price_record = Pricing::where([
            ['gnet_id', $this->gnet->id],
            ['gnet_platform_game_id', $inputs['gnet_platform_id']],
            ['number_person', count($inputs['user_id'])]
        ])->first();

        if (empty($price_record))
            $this->__outPut(['message' => 'تعرفه ای برای این تعداد کاربر و این دستگاه ثبت نشده است'], 400);

        if (!empty($inputs['gnet_custom_game_id']) && !empty($inputs['gnet_game_id']))
            $this->__outPut(['message' => 'شناسه بازی و شناسه بازی شخصی شده نباید با یکدیگر ارسال گردد.'], 400);

        if (count($inputs['user_id']) != $inputs['number_person'])
            $this->__outPut(['message' => 'تعداد کاربران وارد شده باید با تعداد بازیکن ها برابر باشد.'], 400);
    }

    public function remove(Request $request)
    {
        $inputs = $request->all();

        $this->_checkValidation($inputs, [
            'gnet_work_transaction_id' => [
                'required',
                Rule::exists(App(GnetWorkTransaction::class)->getTable(), 'id')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_work_transaction_id']],
                        ['status', '=', GnetWorkTransaction::offline],
                    ]);
                }),
                Rule::exists('gnet_work_transaction_platform')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['gnet_id', '=', $this->gnet->id],
                        ['gnet_work_transaction_id', '=', $inputs['gnet_work_transaction_id']],
                    ]);
                })
            ]
        ]);

        GnetWorkTransaction::find($inputs['gnet_work_transaction_id'])->delete();

        $this->__outPut(['message' => 'زمان بازی با موفقیت حذف شد']);

    }

    public function list(Request $request)
    {
        $inputs = $request->all();

        $this->_checkValidation($inputs, [
            'gnet_platform_id' => [
                'required',
                Rule::exists(App(GnetPlatformGame::class)->getTable(), 'id')->where(function ($query) use ($inputs) {
                    $query->where([
                        ['id', '=', $inputs['gnet_platform_id']],
                        ['gnet_id', '=', $this->gnet->id],
                        ['parent_id', '=', null],
                        ['game_id', '=', null],
                    ]);
                })
            ]
        ]);

        $workTransAction = GnetPlatformGame::where([
            ['id', '=', $inputs['gnet_platform_id']],
            ['gnet_id', '=', $this->gnet->id],
            ['parent_id', '=', null],
            ['game_id', '=', null],
        ])->first()->workTransAction()->with('workUser')->get();

        $this->__outPut($workTransAction);
    }
}
