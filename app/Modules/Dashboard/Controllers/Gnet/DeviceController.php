<?php


namespace App\Modules\Dashboard\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Http\Resources\Dashboard\PlatformListCollection;
use App\Modules\Game_Platform\Models\GnetPlatformGame;
use Illuminate\Http\Request;

class DeviceController extends GameziController
{
    public function platformList()
    {
        $platform_list = $this->gnet->gnetPlatform()->where([
            ['is_active', '=', GnetPlatformGame::Active]
        ])->with(['workTransAction' => function ($query) {
            $query->where('status', 1);
        }, 'workTransAction.workUser'])->get()->toArray();

        $this->__outPut((new PlatformListCollection($platform_list)));
    }

}
