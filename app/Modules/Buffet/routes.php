<?php

use Illuminate\Support\Facades\Route;

$module_namespace = "App\Modules\Buffet\Controllers";

Route::prefix('api')->middleware('api')->namespace($module_namespace)->group(function () {
    Route::prefix('site')->group(function () {
        Route::prefix('food')->group(function () {
            Route::post('/', 'Site\FoodController@insert');
            Route::get('{food}', 'Site\FoodController@get');
            Route::delete('{food}', 'Site\FoodController@delete');
            Route::post('update/{food}', 'Site\FoodController@update');
            Route::post('list', 'Site\FoodController@list');
        });
        Route::prefix('food-category')->group(function () {
            Route::post('/', 'Site\FoodCategoryController@insert');
            Route::get('list', 'Site\FoodCategoryController@list');
            Route::get('{foodCategory}', 'Site\FoodCategoryController@get');
            Route::post('update/{foodCategory}', 'Site\FoodCategoryController@update');
        });
    });

    Route::prefix('client')->group(function () {
        Route::prefix('food')->group(function () {
            Route::post('/', 'Gnet\FoodController@add');
            Route::post('list', 'Gnet\FoodController@list');
            Route::post('detail', 'Gnet\FoodController@get');
            Route::post('remove', 'Gnet\FoodController@remove');
            Route::post('search', 'Gnet\FoodController@search');
        });
        Route::prefix('custom-food')->group(function () {
            Route::post('/', 'Gnet\CustomController@add');
            Route::post('remove', 'Gnet\CustomController@remove');
        });
    });


});
