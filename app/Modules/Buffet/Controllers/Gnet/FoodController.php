<?php


namespace App\Modules\Buffet\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Modules\Buffet\Models\CustomFood;
use App\Modules\Buffet\Models\Food;
use Illuminate\Http\Request;

class FoodController extends GameziController
{
    public function add(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'food' => 'array',
            'food.*.food_id' => 'exists:' . App(Food::class)->getTable() . ',id',
            'food.*.price' => 'required'
        ]);

        if (!empty($inputs['food'])) {
            foreach ($inputs['food'] as $key => $value)
                $food_data[$value['food_id']] = $value;
            $this->gnet->food()->sync($food_data);
        } else
            $this->gnet->food()->detach();

        $this->__outPut(['message' => 'غذاها با موفقیت بروز شد']);
    }

    public function list(Request $request)
    {
        $inputs = $request->all();
        $filter = [
            ['start_price', '>=', 'price'],
            ['end_date', '<=', 'price'],
            ['start_count', '>=', 'count'],
            ['end_count', '<=', 'count'],
        ];
        $condition = $this->_filter($inputs, $filter);
        $food = $this->gnet->food()->where($condition)->paginate(30);
        $this->__outPut($food);
    }

    public function get(Request $request)
    {
        $food = $this->foodFind($request->all());
        $this->__outPut($food);
    }

    public function remove(Request $request)
    {
        $food = $this->foodFind($request->all());
        $this->gnet->food()->detach($food->id);
        $this->__outPut(['message' => 'خوراکی با موفقیت حذف شد']);
    }

    private function foodFind($inputs): object
    {
        $this->_checkValidation($inputs, [
            'food_id' => 'required|exists:' . App(Food::class)->getTable() . ',id'
        ]);
        $food = $this->gnet->food()->find($inputs['food_id']);
        if (empty($food))
            $this->__outPut(['message' => 'خوراکی با این مشخصات پیدا نشد'], 404);
        return $food;
    }

    public function search(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'input' => 'required|string|min:3|max:150'
        ]);

        $data['gnet_food'] = Food::whereHas('gnet', function ($query) {
            $query->where('gnet_id', $this->gnet->id)->where('status', Food::StatusActive);
        })->where('status', Food::StatusActive)
            ->where('title', 'LIKE', '%' . $inputs['input'] . '%')
            ->orWhere('tags', 'LIKE', '%' . $inputs['input'] . '%')
            ->limit(5)
            ->get();

        $data['all_food'] = Food::whereNotIn('id', $data['gnet_food']->pluck('id'))
            ->where('status', Food::StatusActive)
            ->where('title', 'LIKE', '%' . $inputs['input'] . '%')
            ->orWhere('tags', 'LIKE', '%' . $inputs['input'] . '%')
            ->limit(5)
            ->get();

        $data['gnet_custom_food'] = CustomFood::where('gnet_id', $this->gnet->id)
            ->where('title', 'LIKE', '%' . $inputs['input'] . '%')
            ->where('status', Food::StatusActive)
            ->limit(5)
            ->get();

        $this->__outPut($data);
    }

}
