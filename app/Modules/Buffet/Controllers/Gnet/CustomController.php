<?php


namespace App\Modules\Buffet\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Modules\Buffet\Models\CustomFood;
use App\Modules\History\Models\GnetFoodTransaction;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CustomController extends GameziController
{
    public function add(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'gnet_id' => 'required',
            'title' => [
                'required', 'string', 'max:50',
                Rule::unique(App(CustomFood::class)->getTable(), 'title')->where(function ($query) {
                    $query->where('gnet_id', $this->gnet->id);
                })
            ],
            'description' => 'string|max:500|nullable',
            'image' => 'file|mimes:png,jpeg,jpeg|max:3072|nullable',
            'price' => 'numeric|max:10000000000',
            'status' => 'boolean|nullable'
        ]);

        if (isset($inputs['image']))
            $inputs['image'] = $this->_uploadFile($request->file('image'), 'Custom-Food');

        CustomFood::create($inputs);
        $this->__outPut(['message' => 'غذا با موفقیت درج شد']);
    }

    public function remove(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'gnet_id' => 'required',
            'custom_food_id' => [
                'required',
                Rule::exists(App(CustomFood::class)->getTable(), 'id')->where(function ($query) {
                    $query->where('gnet_id', $this->gnet->id);
                })]
        ]);

        $custom_food = CustomFood::find($inputs['custom_food_id']);
        $work_food = $custom_food->workFood()->where('paid', GnetFoodTransaction::FlgUnpaid)->count();
        if ($work_food > 0)
            $this->__outPut(['message' => ' خوراکی  ' . $work_food . ' برای یک کاربر ثبت شده که پرداخت نشده است.'], 409);

        $custom_food->delete();
        $this->__outPut(['message' => 'خوراکی با موفقیت حذف شد']);
    }

}
