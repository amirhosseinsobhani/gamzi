<?php


namespace App\Modules\Buffet\Controllers\Site;


use App\Http\Controllers\GameziController;
use App\Modules\Buffet\Models\Food;
use App\Modules\Buffet\Models\FoodCategory;
use Illuminate\Http\Request;

class FoodController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }

    public function insert(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'title' => 'required|string|unique:' . App(Food::class)->getTable() . ',title',
            'category_id' => 'exists:' . App(FoodCategory::class)->getTable() . ',id',
            'description' => 'string',
            'image' => 'file|mimes:png,jpeg,jpeg|max:3072',
            'tags' => 'string'
        ]);
        if (isset($inputs['image']))
            $inputs['image'] = $this->_uploadFile($request->file('image'), 'Food');

        $food = Food::create($inputs);
        $food->category()->sync($inputs['category_id']);

        $this->__outPut(['message' => 'غذا با موفقیت درج شد']);
    }

    public function get(Food $food)
    {
        $this->__outPut($food->load('category'));
    }

    public function update(Food $food, Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'title' => 'required|string',
            'category_id' => 'exists:' . App(FoodCategory::class)->getTable() . ',id',
            'description' => 'string',
            'image' => 'file|mimes:png,jpeg,jpeg|max:3072',
            'tags' => 'string'
        ]);
        if (isset($inputs['image']))
            $inputs['image'] = $this->_uploadFile($request->file('image'), 'Food');

        $food->update($inputs);
        $food->category()->sync($inputs['category_id']);

        $this->__outPut(['message' => 'غذا با موفقیت درج شد']);
    }

    public function delete(Food $food)
    {
        if (count($food->gnet) > 0)
            $this->__outPut(['message' => 'غذای مورد نظر به گیمنتی اختصاص داده شده است و امکان حذف ندارد'], 409);

        $food->delete();

        $this->__outPut(['message' => 'غذا با موفقیت حذف شد']);
    }

    public function list(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'category_id' => 'exists:' . App(FoodCategory::class)->getTable() . ',id',
            'title' => 'string'
        ]);
        $filter = [
            ['category_id', '='],
            ['title', 'like']
        ];
        $condition = $this->_filter($inputs, $filter);
        $games = Food::where($condition)->paginate(20);
        $this->__outPut($games);
    }
}
