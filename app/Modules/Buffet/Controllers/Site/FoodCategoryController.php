<?php


namespace App\Modules\Buffet\Controllers\Site;


use App\Http\Controllers\GameziController;
use App\Http\Traits\Who;
use App\Modules\Buffet\Models\FoodCategory;
use Illuminate\Http\Request;

class FoodCategoryController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }

    public function insert(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'title' => 'required|string|unique:' . App(FoodCategory::class)->getTable() . ',title',
            'icon' => 'file|mimes:png,jpeg,jpeg|max:3072'
        ]);
        if (isset($inputs['icon']))
            $inputs['icon'] = $this->_uploadFile($request->file('icon'), 'Food-Category');

        FoodCategory::create($inputs);
        $this->__outPut(['message' => 'دسته بندی با موفقیت ثبت شد']);
    }

    public function list()
    {
        $food_category = FoodCategory::all();
        $this->__outPut($food_category);
    }

    public function get(FoodCategory $foodCategory)
    {
        $this->__outPut($foodCategory);
    }

    public function update(FoodCategory $foodCategory, Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'title' => 'required|string',
            'icon' => 'file|mimes:png,jpeg,jpeg|max:3072'
        ]);
        if (isset($inputs['icon']))
            $inputs['icon'] = $this->_uploadFile($request->file('icon'), 'Food-Category');

        $foodCategory->update($inputs);
        $this->__outPut(['message' => 'بروزرسانی با موفقیت انجام شد']);
    }
}
