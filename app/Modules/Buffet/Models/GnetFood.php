<?php


namespace App\Modules\Buffet\Models;


use App\Http\Models\GModel;
use App\Modules\Client\Models\Gnet;

class GnetFood extends GModel
{
    protected $table = 'gnet_food';

    protected $fillable = [
        'food_id',
        'gnet_id',
        'price',
        'count',
        'status',
    ];

    public function gnet()
    {
        return $this->belongsTo(Gnet::class, 'gnet_id');
    }

    public function food()
    {
        return $this->belongsTo(Food::class, 'food_id');
    }

}
