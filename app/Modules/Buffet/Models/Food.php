<?php


namespace App\Modules\Buffet\Models;


use App\Modules\Client\Models\Gnet;
use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $table = 'food';

    const StatusActive = 1, StatusDeactive = 0;

    protected $fillable = [
        'title',
        'description',
        'status',
        'image',
        'tags',
    ];

    public function category()
    {
        return $this->belongsToMany(FoodCategory::class, 'food_category_detail' , 'food_id');
    }

    public function gnet()
    {
        return $this->belongsToMany(Gnet::class, 'gnet_food', 'food_id');
    }
}
