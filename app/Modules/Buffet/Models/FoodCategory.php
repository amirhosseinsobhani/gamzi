<?php


namespace App\Modules\Buffet\Models;


use Illuminate\Database\Eloquent\Model;

class FoodCategory extends Model
{
    protected $table = 'food_category';

    protected $fillable = [
        'title'
    ];

    public function game()
    {
        $this->belongsToMany(Food::class, 'food_category_detail', 'food_category_id');
    }
}
