<?php


namespace App\Modules\Buffet\Models;


use App\Modules\Client\Models\Gnet;
use App\Modules\History\Models\GnetFoodTransaction;
use Illuminate\Database\Eloquent\Model;

class CustomFood extends Model
{
    protected $table = 'gnet_custom_food';



    protected $fillable = [
        'gnet_id',
        'title',
        'description',
        'status',
        'image',
        'admin_status',
        'price',
    ];

    public function gnet()
    {
        return $this->belongsTo(Gnet::class);
    }

    public function workFood()
    {
        return $this->hasMany(GnetFoodTransaction::class, 'gnet_custom_food_id');
    }

}
