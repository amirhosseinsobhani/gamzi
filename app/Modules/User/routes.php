<?php

use Illuminate\Support\Facades\Route;

$module_namespace = "App\Modules\User\Controllers";

Route::prefix('api')->middleware('api')->namespace($module_namespace)->group(function () {
    Route::prefix('site')->group(function () {
        Route::prefix('user')->group(function () {
            Route::post('list', 'Site\UserController@list');
            Route::get('{user}', 'Site\UserController@get');
            Route::patch('{user}', 'Site\UserController@update');
        });
    });

    Route::prefix('client')->group(function () {
        Route::prefix('user')->group(function () {
            Route::post('register', 'Gnet\UserController@register');
            Route::post('verify', 'Gnet\UserController@verify');
            Route::post('login', 'Gnet\UserController@login');
            Route::post('reset-pass', 'Gnet\UserController@resetPass');
        });

        Route::prefix('manage-user')->group(function () {
            Route::post('add', 'Gnet\ManageUserController@addMember');
            Route::post('remove', 'Gnet\ManageUserController@removeMember');
            Route::post('list','Gnet\ManageUserController@memberList');
            Route::post('search', 'Gnet\ManageUserController@searchMember');
        });
    });
});
