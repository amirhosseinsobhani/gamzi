<?php


namespace App\Modules\User\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static generate($id)
 */
class Token extends Model
{
    protected $table = 'user_token';

    protected $fillable = [
        'user_id',
        'token',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeGenerate($query, int $user_id)
    {
        $token_count = Token::where('user_id', $user_id)->count();

        if ($token_count > 5)
            Token::where('user_id', $user_id)->orderBy('id', 'ASC')->take($token_count - 5)->delete();

        $carbon = new Carbon();
        $expireDate = $carbon->addYears(1)->toDateTimeString();
        $mainData = [
            'expire' => $expireDate,
            'user' => $user_id,
        ];

        $token = encrypt($mainData);
        $data = [
            'token' => md5($token),
            'user_id' => $user_id,
        ];

        $res = self::create($data);
        return !$res ? false : $token;
    }

    public static function isValid($token)
    {
        $rowToken = self::where('token', md5($token))->first();
        if (empty($rowToken))
            return false;

        $mainToken = decrypt($token);
        if (empty($mainToken['user'])) {
            return false;
        }
        if (empty($mainToken['expire'])) {
            return false;
        }

        $expire = $mainToken['expire'];
        $expireDate = new Carbon($expire);
        if ($expireDate->isPast())
            return false;

        if ($rowToken['user_id'] != $mainToken['user'])
            return false;

        return true;
    }

}
