<?php


namespace App\Modules\User\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * @method static generate($id, string $type)
 */
class Verify extends Model
{
    protected $table = 'user_verify';

    protected $fillable = [
        'user_id',
        'code',
        'type'
    ];

    const MobileType = 0, EmailType = 1;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeGenerate($query, int $user_id, string $type)
    {
        $type_id = $type == 'email' ? self::EmailType : self::MobileType;
        $code = rand(1000, 9999);
        $data = [
            'user_id' => $user_id,
            'code' => $code,
            'type' => $type_id
        ];

        self::where([['user_id', $user_id], ['type', $type_id]])->delete();
        return self::updateOrCreate($data);
    }

}
