<?php


namespace App\Modules\User\Models;


use App\Http\Models\GModel;
use App\Modules\Client\Models\Gnet;

/**
 * @method static emailOrMobile(array $inputs)
 * @method static getUserType(array $inputs)
 */
class User extends GModel
{
    protected $table = 'user';

    protected $fillable = [
        'user_role_id',
        'name',
        'family',
        'mobile',
        'email',
        'password',
        'flg_active',
        'flg_mobile_active',
        'flg_email_active',
    ];

    protected $hidden = [
        'password'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function token()
    {
        return $this->hasMany(Token::class);
    }

    public function verify()
    {
        return $this->hasMany(Verify::class);
    }

    public function gnet()
    {
        return $this->hasMany(Gnet::class);
    }

    public function scopeEmailOrMobile($query, array $data)
    {
        if (!empty($data['email']) && !empty($data['mobile']))
            $this->__outPut(['message' => 'موبایل و پست الکترونیکی نباید با یکدیگر ارسال گردد.'], 400);
        return true;
    }

    public function scopeGetUserType($query, array $data)
    {
        return $type = !empty($data['email']) ? ['type' => 'email', 'type_id' => Verify::EmailType]
            : ['type' => 'mobile', 'type_id' => Verify::MobileType];
    }

    static function getByUsername($username)
    {
        $usernameField = User::detectUsernameType($username);
        if (empty($usernameField))
            return null;
        $user = User::where($usernameField, $username)->first();
        if (empty($user))
            return null;
        else
            return $user;
    }

    static function detectUsernameType($username)
    {
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $type = 'email';
        } else if (preg_match('/(09)[0-9]{9}/', $username, $output_array) && strlen($username) == 11) {
            $type = 'mobile';
        } else {
            $type = null;
        }
        return $type;
    }

}
