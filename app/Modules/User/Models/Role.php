<?php


namespace App\Modules\User\Models;


use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'user_role';

    protected $fillable = [
        'alias',
        'name',
        'description',
    ];

    public function permission()
    {
        return $this->belongsToMany(Permission::class, 'user_permission_role', 'user_role_id', 'user_permission_id');
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }

}
