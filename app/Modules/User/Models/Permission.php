<?php


namespace App\Modules\User\Models;


use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'user_permission';

    protected $fillable = [
        'parent_id',
        'alias',
        'name',
        'description',
    ];

    public function role()
    {
        return $this->belongsToMany(Role::class, 'user_permission_role', 'user_permission_id', 'user_role_id');
    }
}
