<?php


namespace App\Modules\User\Models;


use App\Modules\Client\Models\Gnet;
use Illuminate\Database\Eloquent\Model;

class MemberShip extends Model
{
    protected $table = 'gnet_membership';

    protected $fillable = [
        'gnet_id',
        'user_id',
    ];

    public function gnet()
    {
        return $this->belongsTo(Gnet::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
