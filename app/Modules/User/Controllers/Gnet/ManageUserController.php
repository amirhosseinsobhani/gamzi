<?php


namespace App\Modules\User\Controllers\Gnet;


use App\Http\Controllers\GameziController;
use App\Modules\Client\Models\Gnet;
use App\Modules\User\Models\MemberShip;
use App\Modules\User\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ManageUserController extends GameziController
{
    public function addMember(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'user_id' => [
                'required',
                'exists:' . App(User::class)->getTable() . ',id',
                Rule::unique(App(MemberShip::class)->getTable())->where(function ($query) {
                    $query->where('gnet_id', $this->gnet->id);
                })
            ]
        ]);

        MemberShip::create($inputs);
        $this->__outPut(['message' => 'کاربر با موفقیت افزوده شد.']);
    }

    public function removeMember(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'user_id' => 'required|exists:' . App(User::class)->getTable() . ',id'
        ]);

        MemberShip::where($inputs)->delete();
        $this->__outPut(['message' => 'کاربر با موفقیت حذف شد.']);
    }

    public function memberList(Request $request)
    {
        $membership = MemberShip::where('gnet_id', $this->gnet->id)->with('user')->paginate(25);
        $this->__outPut($membership);
    }

    public function searchMember(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'input' => 'required|string|min:5|max:150'
        ]);

        $data['gnet_member'] = Gnet::find($inputs['gnet_id'])
            ->member()->where('email', 'like', '%' . $inputs['input'] . '%')
            ->orWhere('mobile', 'like', '%' . $inputs['input'] . '%')
            ->orWhere(DB::raw('CONCAT(name, " ", family)'), 'like', '%' . $inputs['input'] . '%')
            ->limit(5)
            ->get();


        $data['all_user'] = User::where(function ($query) use ($inputs) {
            $query->where('email', 'like', '%' . $inputs['input'] . '%')
                ->orWhere('mobile', 'like', '%' . $inputs['input'] . '%')
                ->orWhere(DB::raw('CONCAT(name, " ", family)'), 'like', '%' . $inputs['input'] . '%');
        })->whereNotIn('id', $data['gnet_member']->pluck('id'))
            ->limit(5)
            ->get();

        $this->__outPut($data);
    }

}
