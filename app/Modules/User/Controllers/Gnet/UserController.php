<?php

namespace App\Modules\User\Controllers\Gnet;


use App\Http\Controllers\Controller;
use App\Mail\ResetPassword;
use App\Mail\VerifyEmail;
use App\Modules\User\Models\Role;
use App\Modules\User\Models\Token;
use App\Modules\User\Models\User;
use App\Modules\User\Models\Verify;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            "name" => "required|string|max:50",
            "family" => "required|string|max:50",
            "email" => "required_without:mobile|string|max:150|email:rfc,dns|unique:" . App(User::class)->getTable() . ",email",
            "mobile" => "required_without:email|string|phone|unique:" . App(User::class)->getTable() . ",mobile",
            "password" => "required|string|max:50|min:5",
        ]);

        User::emailOrMobile($inputs);

        $role = Role::where('alias', 'public')->first();
        empty($role) ? $this->__outPut(['message' => 'the public role not exist'], 404) : true;

        $inputs['password'] = Hash::make($inputs['password']);
        $user = User::create(array_merge($inputs, ['user_role_id' => $role->id]));

        $type = User::GetUserType($inputs);
        $verify = Verify::generate($user->id, $type['type']);
        $text = 'activate code is : ';

        if ($type['type'] == 'email')
            Mail::to($user->email)->send(new VerifyEmail($verify, $text));

        $this->__outPut(['message' => 'کد فعال سازی ارسال شد.']);
    }

    public function verify(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'email' => 'required_without:mobile|string|max:150|email:rfc,dns',
            'mobile' => 'required_without:email|string|phone',
            'code' => 'required|between:1,5'
        ]);

        User::emailOrMobile($inputs);

        $type = User::getUserType($inputs);

        if ($type['type'] == 'email')
            $user = User::where('email', $inputs['email'])->first();
        elseif ($type['type'] == 'mobile')
            $user = User::where('mobile', $inputs['mobile'])->first();

        $verify = $user->verify()->where([
            ['type', '=', $type['type_id']],
            ['code', '=', $inputs['code']],
            ['created_at', '>', Carbon::now()->subMinute(4)->format('Y-m-d H:i:s')],
        ])->orderBy('id', 'ASC')->first();

        if (empty($verify))
            $this->__outPut(['message' => 'کدفعال سازی معتبر نمی باشد.'], 400);

        $user->update([
            'flg_active' => 1,
            'flg_' . $type['type'] . '_active' => 1,
        ]);

        $verify->delete();
        $token = Token::generate($user->id);
        $this->__outPut(['message' => 'احراز هویت موفقیت آمیز بود', 'data' => $token]);
    }

    public function login(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'email' => 'required_without:mobile|string|max:150|email',
            'mobile' => 'required_without:email|string|phone',
            'password' => 'required|max:100'
        ]);
        User::emailOrMobile($inputs);
        $type = User::getUserType($inputs);

        $condition = $this->_condition($inputs, [$type['type']]);
        $user = User::where($condition)->first();
        if (empty($user))
            $this->__outPut(['message' => 'کاربری با این مشخصات وجود ندارد'], 404);
        if (Hash::check($inputs['password'], $user->password)) {
            $token = Token::generate($user->id);
            $this->__outPut(['message' => 'با موفقیت وارد شدید.', $token]);
        }
        $this->__outPut(['message' => 'رمز عبور اشتباه است.'], 401);
    }

    public function resetPass(Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            'email' => 'required_without:mobile|string|max:150|email:rfc,dns',
            'mobile' => 'required_without:email|string|phone'
        ]);
        User::emailOrMobile($inputs);
        $new_pass = Str::random(6);

        $type = User::getUserType($inputs);
        $condition = $this->_condition($inputs, [$type['type']]);
        $user = User::where($condition)->first();
        if (empty($user))
            $this->__outPut(['message' => 'کاربری با این مشخصات وجود ندارد'], 404);
        if ($user->flg_active != 1)
            $this->__outPut(['message' => 'کاربر مورد نظر فعال نیست.'], 401);

        $user->update([
            'password' => Hash::make($new_pass)
        ]);
        $text = 'new password is : ';
        if ($type['type'] == 'email')
            Mail::to($user->email)->send(new ResetPassword($new_pass, $text));
        $this->__outPut(['message' => 'رمز عبور جدید ارسال شد.']);
    }

}
