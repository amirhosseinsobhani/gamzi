<?php


namespace App\Modules\User\Controllers\Site;


use App\Http\Controllers\GameziController;
use App\Modules\User\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends GameziController
{
    public function __construct()
    {
        $this->gnet_id_required = false;
        parent::__construct();
    }

    public function list(Request $request)
    {
        $inputs = $request->all();
        $filter = [
            ['name', 'like'],
            ['family', 'like'],
            ['email', 'like'],
            ['mobile', 'like'],
            ['role_id', '='],
        ];
        $condition = $this->_filter($inputs, $filter);
        $user = User::where($condition)->paginate(20);
        $this->__outPut($user);
    }

    public function get(User $user)
    {
        $this->__outPut($user);
    }

    public function delete(User $user)
    {
        $user->delete();
        $this->__outPut(['message' => 'کاربر با موفقیت حذف شد']);
    }

    public function update(User $user, Request $request)
    {
        $inputs = $request->all();
        $this->_checkValidation($inputs, [
            "name" => "max:50",
            "family" => "max:50",
            "email" => "max:150|email:rfc,dns|unique:" . App(User::class)->getTable() . ",email",
            "mobile" => "phone|unique:" . App(User::class)->getTable() . ",mobile",
            "password" => "required|string|max:50|min:5"
        ]);
        $inputs['password'] = Hash::make($inputs['password']);
        $user->update($inputs);
        $this->__outPut(['message' => 'کاربر با موفقیت بروز شد.']);
    }

}
