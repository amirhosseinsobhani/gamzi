<?php

use Illuminate\Support\Facades\Route;

$module_namespace = "App\Modules\History\Controllers";

Route::prefix('api')->middleware('api')->namespace($module_namespace)->group(function () {
    Route::prefix('site')->group(function () {

    });

    Route::prefix('client')->group(function () {

    });
});
