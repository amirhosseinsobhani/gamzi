<?php


namespace App\Modules\History\Models;


use Illuminate\Database\Eloquent\Model;

class GnetFoodTransaction extends Model
{
    protected $table = 'gnet_food_transaction';

    const FlgPaid = 0, FlgUnpaid = 1;

    protected $fillable = [
        'gnet_work_transaction_id',
        'gnet_membership_id',
        'user_alias',
        'gnet_food_id',
        'gnet_custom_food_id',
        'amount',
        'paid',
    ];

}
