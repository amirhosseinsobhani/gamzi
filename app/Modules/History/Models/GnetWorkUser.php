<?php


namespace App\Modules\History\Models;


use Illuminate\Database\Eloquent\Model;

class GnetWorkUser extends Model
{
    protected $table = 'gnet_work_user';

    protected $fillable = [
        'gnet_work_transaction_id',
        'amount',
        'paid',
        'start_time',
        'end_time',
    ];

    public $timestamps = false;

    public function workTransaction()
    {
        return $this->belongsTo(GnetWorkTransaction::class, 'gnet_work_transaction_id');
    }

}
