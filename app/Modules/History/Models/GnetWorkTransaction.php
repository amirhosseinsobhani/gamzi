<?php


namespace App\Modules\History\Models;


use App\Http\Models\GModel;
use App\Modules\Client\Models\Gnet;
use App\Modules\Game_Platform\Models\CustomGame;
use App\Modules\Game_Platform\Models\GnetPlatformGame;
use App\Modules\User\Models\User;

class GnetWorkTransaction extends GModel
{
    protected $table = 'gnet_work_transaction';

    const offline = 0, online = 1;

    protected $fillable = [
        'gnet_game_id',
        'gnet_custom_game_id',
        'total_amount',
        'amount_paid',
        'status',
        'start_time',
        'end_time',
    ];

    public function workUser()
    {
        return $this->belongsToMany(User::class, 'gnet_work_user', 'gnet_work_transaction_id', 'user_id')
                    ->withPivot('paid');
    }

    public function workPlatform()
    {
        return $this->belongsToMany(GnetPlatformGame::class, 'gnet_work_transaction_platform', 'gnet_work_transaction_id', 'gnet_platform_id')
            ->withPivot('gnet_id');
    }

    public function gnet()
    {
        return $this->belongsToMany(Gnet::class, 'gnet_work_transaction_platform', 'gnet_work_transaction_id', 'gnet_id')
            ->withPivot('gnet_platform_id');
    }

    public function game()
    {
        return $this->belongsTo(GnetPlatformGame::class, 'gnet_game_id');
    }

    public function customGame()
    {
        return $this->belongsTo(CustomGame::class, 'gnet_custom_game_id');
    }

}
