<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGnetWorkTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gnet_work_transaction', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('gnet_game_id')->nullable();
            $table->foreign('gnet_game_id')->on('gnet_platform_game')->references('id');
            $table->unsignedBigInteger('gnet_custom_game_id')->nullable();
            $table->foreign('gnet_custom_game_id')->on('gnet_custom_game')->references('id');
            $table->integer('total_amount')->default(0);
            $table->integer('amount_paid')->default(0);
            $table->boolean('status')->default(1)->comment('offline=0, online=1');
            $table->string('start_time');
            $table->string('end_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gnet_work_transaction');
    }
}
