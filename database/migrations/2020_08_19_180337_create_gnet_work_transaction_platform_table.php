<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGnetWorkTransactionPlatformTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gnet_work_transaction_platform', function (Blueprint $table) {
            $table->unsignedBigInteger('gnet_id');
            $table->foreign('gnet_id')->on('gnet')->references('id')->onDelete('cascade');
            $table->unsignedBigInteger('gnet_platform_id');
            $table->foreign('gnet_platform_id')->on('gnet_platform_game')->references('id')->onDelete('cascade');
            $table->unsignedBigInteger('gnet_work_transaction_id');
            $table->foreign('gnet_work_transaction_id')->on('gnet_work_transaction')->references('id')->onDelete('cascade');
            $table->unique(['gnet_id', 'gnet_platform_id', 'gnet_work_transaction_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gnet_work_transaction_platform');
    }
}
