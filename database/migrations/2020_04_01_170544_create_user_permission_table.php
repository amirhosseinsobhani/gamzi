<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_permission', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')->on('user_permission')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->string('alias', 50);
            $table->string('name', 50)->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('user_permission_role', function (Blueprint $table) {
            $table->unsignedBigInteger('user_role_id');
            $table->foreign('user_role_id')->on('user_role')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('user_permission_id');
            $table->foreign('user_permission_id')->on('user_permission')->references('id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_permission');
        Schema::dropIfExists('user_permission_role');
    }
}
