<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGnetPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gnet_permission', function (Blueprint $table) {
            $table->id();
            $table->string('alias');
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->unsignedBigInteger('parent_id');
            $table->foreign('parent_id')->on('gnet_permission')->references('id')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('gnet_user_permission', function (Blueprint $table) {
            $table->unsignedBigInteger('gnet_membership_id');
            $table->foreign('gnet_membership_id')->on('gnet_membership')->references('id');
            $table->unsignedBigInteger('gnet_id');
            $table->foreign('gnet_id')->on('gnet')->references('id')->onDelete('cascade');
            $table->unsignedBigInteger('gnet_permission_id');
            $table->foreign('gnet_permission_id')->on('gnet_permission')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gnet_permission');
        Schema::dropIfExists('gnet_user_permission');
    }
}
