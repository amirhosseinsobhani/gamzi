<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGnetWorkFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gnet_food_transaction', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('gnet_work_transaction_id');
            $table->foreign('gnet_work_transaction_id')->on('gnet_work_transaction')->references('id')->onDelete('cascade');
            $table->unsignedBigInteger('gnet_food_id')->nullable();
            $table->foreign('gnet_food_id')->on('gnet_food')->references('id');
            $table->unsignedBigInteger('gnet_custom_food_id')->nullable();
            $table->foreign('gnet_custom_food_id')->on('gnet_custom_food')->references('id');
            $table->integer('amount')->default(0);
            $table->integer('paid')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gnet_food_transaction');
    }
}
