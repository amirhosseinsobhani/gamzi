<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGnetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gnet', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->on('user')->references('id');
            $table->unsignedBigInteger('province_city_id');
            $table->foreign('province_city_id')->on('province_city')->references('id');
            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('site')->nullable();
            $table->string('address')->nullable();
            $table->string('image')->nullable();
            $table->string('work_start_time')->nullable();
            $table->string('work_end_time')->nullable();
            $table->boolean('flg_active')->default(1);
            $table->boolean('flg_buffet_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gnet');
    }
}
