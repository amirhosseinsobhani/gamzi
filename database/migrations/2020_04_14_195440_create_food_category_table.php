<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_category', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('icon')->nullable();
            $table->timestamps();
        });

        Schema::create('food_category_detail', function (Blueprint $table) {
            $table->unsignedBigInteger('food_id');
            $table->foreign('food_id')->on('food')->references('id')->onDelete('cascade');
            $table->unsignedBigInteger('food_category_id');
            $table->foreign('food_category_id')->on('food_category')->references('id')->onDelete('cascade');
            $table->primary(['food_id', 'food_category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_category');
        Schema::dropIfExists('food_category_detail');
    }
}
