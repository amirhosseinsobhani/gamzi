<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGnetInboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gnet_inbox', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('gnet_id');
            $table->foreign('gnet_id')->on('gnet')->references('id')->onDelete('cascade');
            $table->string('subject');
            $table->text('content');
            $table->boolean('flg_read')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gnet_inbox');
    }
}
