<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->tinyInteger('type')->comment('image=0, video=1');
            $table->string('url');
            $table->string('preview')->nullable();
            $table->timestamps();
        });

        Schema::create('media_game', function (Blueprint $table) {
            $table->unsignedBigInteger('game_id');
            $table->foreign('game_id')->on('game')->references('id')->onDelete('cascade');
            $table->unsignedBigInteger('media_id');
            $table->foreign('media_id')->on('media')->references('id')->onDelete('cascade');
            $table->primary(['game_id', 'media_id']);
        });

        Schema::create('gnet_media', function (Blueprint $table) {
            $table->unsignedBigInteger('gnet_id');
            $table->foreign('gnet_id')->on('gnet')->references('id')->onDelete('cascade');
            $table->unsignedBigInteger('media_id');
            $table->foreign('media_id')->on('media')->references('id')->onDelete('cascade');
            $table->primary(['gnet_id', 'media_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_game');
        Schema::dropIfExists('gnet_media');
        Schema::dropIfExists('media');
    }
}
