<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGnetWorkPlatformTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gnet_work_user', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('gnet_work_transaction_id');
            $table->foreign('gnet_work_transaction_id')->on('gnet_work_transaction')->references('id')->onDelete('cascade');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->on('user')->references('id')->onDelete('cascade');
            $table->integer('paid')->default(0);
            $table->string('start_time');
            $table->string('end_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gnet_work_user');
    }
}
