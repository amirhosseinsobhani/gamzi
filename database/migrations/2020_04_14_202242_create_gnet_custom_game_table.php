<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGnetCustomGameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gnet_custom_game', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('gnet_id');
            $table->foreign('gnet_id')->on('gnet')->references('id')->onDelete('cascade');
            $table->string('company')->nullable();
            $table->string('alias');
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('age_rating')->nullable();
            $table->tinyInteger('number_player')->nullable();
            $table->tinyInteger('admin_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gnet_custom_game');
    }
}
